// initialize the map
var map = L.map('map').setView([51.3288, 12.371], 14);

// load a tile layer
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
  {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>, ' + ' <a href="http://opendatacommons.org/licenses/odbl/">ODbL</a>'

  }).addTo(map);
  var marker = L.marker([51.33241, 12.36854]).addTo(map);
  marker.bindPopup("<b>Bibliotheca Albertina</b><br>Beethovenstr. 6<br>04107 Leipzig");

  var m1 = L.marker([51.338497, 12.378441]).addTo(map);
  m1.bindPopup("<b>Campus-Bibliothek</b><br> Universitätsstr. 3 (im Hoersaalgebaeude)<br>04109 Leipzig");

  var m2 = L.marker([51.337744,12.373642]).addTo(map);
  m2.bindPopup("<b>Bibliothek Rechtswissenschaften</b><br>Burgstr. 27 <br>04109 Leipzig");

  var m3 = L.marker([51.331768,12.381921]).addTo(map);
  m3.bindPopup("<b>Bibliothek Medizin/Naturwissenschaften</b><br>Liebigstr. 23/25 <br>04103 Leipzig");

  var m4 = L.marker([51.333773, 12.367756]).addTo(map);
  m4.bindPopup("<b>Bibliothek­­ Deutsches­ Literatur­institut</b><br>Wächterstr. 34 <br>04107 Leipzig");

  var m5 = L.marker([51.33831, 12.35472]).addTo(map);
  m5.bindPopup("<b>Bibliothek­ Erziehungs- und Sportwissenschaft</b><br>Marschnerstr. 29E (Haus 5) <br>04109 Leipzig");

  var m6 = L.marker([51.340706,12.379565]).addTo(map);
  m6.bindPopup("<b>Bibliothek­ Klassische­ Archäologie und ­Ur- und Früh­geschichte</b><br>Ritterstr. 14<br>04109 Leipzig");

  var m7 = L.marker([51.341286,12.371501]).addTo(map);
  m7.bindPopup("<b>Bibliothek Kunst</b><br>Dittrichring 18–20<br>04109 Leipzig");

  var m8 = L.marker([51.338459, 12.376985]).addTo(map);
  m8.bindPopup("<b>Bibliothek Musik</b><br>Neumarkt 9–19 – Aufgang D <br> Städtisches Kaufhaus <br>04109 Leipzig");

  var m9 = L.marker([51.337201, 12.377536]).addTo(map);
  m9.bindPopup("<b>Bibliothek Orient­­­wis­sen­­­schaf­ten</b><br>Schillerstr. 6<br>04109 Leipzig");

  var m10 = L.marker([51.320655,12.390906]).addTo(map);
  m10.bindPopup("<b>Bibliothek Veterinärmedizin</b><br>An den Tierkliniken 5<br>04103 Leipzig");
