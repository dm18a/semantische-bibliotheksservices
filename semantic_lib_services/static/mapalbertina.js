var maps = L.map('maps', {
  minZoom: 1,
  maxZoom: 5,
  crs: L.CRS.Simple
}).setView([-131.5, 135.5], 1);


var southWest = maps.unproject([0,8192], maps.getMaxZoom());
var northEast = maps.unproject([8192, 0], maps.getMaxZoom());
maps.setMaxBounds(new L.LatLngBounds(southWest, northEast));


L.tileLayer('/static/map-tile/{z}/map_{x}_{y}.png', {

        attribution: 'Albertina',
        tms: true
    }).addTo(maps);


//var circle = L.circle([-130.134436, 170.5],20).addTo(maps);

  var popup = L.popup();

  //Funktion die Koordinaten auf Click angibt
function onMapClick(e) {
popup
    .setLatLng(e.latlng)
    .setContent("You clicked the map at " + e.latlng.toString())
    .openOn(maps);
}

maps.on('click', onMapClick);
