from django.urls import path
from django.views.generic import TemplateView

from .views import show_book_info, show_map

urlpatterns = [
    path('mapalbertina/<str:ppn>', show_map),
    path('<str:id>', show_book_info),
]
