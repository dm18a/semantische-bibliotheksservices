"""Creates and renders the detail page of a book."""
from django.conf import settings
from django.shortcuts import render

from semantic_lib_services.helpers import get_albertina_coordinates, load_book_information, get_book_information_from_wachtl


def show_book_info(request, id: str):
    """Gathers the information about a book and renders the page."""

    # use the search function to gather the information for a single book, use only the first result
    book_details = load_book_information([{'id': id}])['results']
    book_details = list(book_details.values())[0]

    return render(request, 'title/book.html', {'book_details': book_details})


def show_map(request, ppn: str):
    """Gather the details about a book position and renders the map-page."""
    # TODO: use a more generic template

    book_info = get_book_information_from_wachtl(settings.WACHTL_URL + ppn)
    positions = get_position_in_lib(book_info)
    for library in positions:
        if library == 'Bibliotheca Albertina':
            for room_number, room in enumerate(positions[library]):
                positions[library][room_number] = get_albertina_coordinates(room)
            return render(
                    request,
                    'title/mapalbertina.html',
                    {'library': positions[library], 'errors': book_info['errors']}
            )

    # render fallback page without position information
    return render(request, 'title/mapalbertina.html', {'library': {}, 'errors': book_info['errors']})


def get_position_in_lib(book_info):
    """
    Use the ppn and a position dictionary to determine where in a library a book is.

    The position_dict is supposed to have this format: {library: signature}
    Implemented at the moment is just the Bibliotheca Albertina.
    Returns a similar dict, following this structure: {library: pos_code}
    """
    position = {}

    for library in book_info['libraries']:
        if library == 'Bibliotheca Albertina':
            # cut signature, so it just shows the area it is in
            pos_code = book_info['libraries'][library]['book_signature'][:2]
            # match the signature with the corresponding place in the library
            if pos_code[0] == 'B':
                position[library] = ['1ogM', '2ogM']
            elif pos_code[0] in ['I', 'F', 'N']:
                position[library] = ['1ogM']
            elif (pos_code == 'Ku') or (pos_code[1] == 'C'):
                position[library] = ['3ogO']
            elif (pos_code == 'Af') or (pos_code[1] in ['D', 'K']):
                position[library] = ['OffM']
            elif pos_code == 'AH':
                position[library] = ['2ogW', '2ogM', '2ogO']
            elif pos_code[0] in ['A', 'H', 'L']:
                position[library] = ['2ogW']
            elif pos_code[0] in ['E', 'G']:
                position[library] = ['2ogO']
            elif pos_code in ['MA', 'MB', 'MC', 'MD', 'ME', 'MF', 'MG', 'MH', 'MI', 'MJ', 'MK', 'ML']:
                position[library] = ['3ogM']
            elif pos_code[0] in ['M', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Z']:
                position[library] = ['3ogW']
            elif pos_code.isdigit():
                position[library] = ['Maga']
            else:
                raise Exception("Couldn't match signature")

    return position
