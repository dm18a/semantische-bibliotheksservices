from django.test import TestCase
from title.views import get_position_in_lib
from semantic_lib_services.helpers import wachtl_query


class TestLibPositions(TestCase):
    def test_get_position_in_lib(self):
        self.assertEqual(get_position_in_lib(
            {"libraries": {'Bibliotheca Albertina': {'book_position': 'Freihandbereich '
                                                                      'Theologie',
                                                     'book_signature': 'BQ 4900 W829',
                                                     'book_status': 'ausleihbar',
                                                     'city': '04107 Leipzig',
                                                     'coordinates': [51.33241, 12.36854],
                                                     'link': 'https://goo.gl/maps/6rJrZgKMW8M2',
                                                     'name': 'Bibliotheca Albertina',
                                                     'street': 'Beethovenstr. 6',
                                                     'variable': '1'}},
             "errors": [],
             "link": "https://katalog.ub.uni-leipzig.de/Record/0007894383"}),
            {'Bibliotheca Albertina': ['1ogM', '2ogM']})
        self.assertEqual(get_position_in_lib(
            {"libraries": {'Orientwissenschaften': {'book_position': 'Orient ',
                                                    'book_signature': 'Kab 23',
                                                    'book_status': 'Präsenz',
                                                    'city': '04109 Leipzig',
                                                    'coordinates': [51.337201, 12.377536],
                                                    'link': 'https://goo.gl/maps/7Cschch9kGE2',
                                                    'name': 'Bibliothek Orientwissenschaften',
                                                    'street': 'Schillerstr. 6',
                                                    'variable': '9'}},
             "errors": [],
             "link": "https://katalog.ub.uni-leipzig.de/Record/0004314209"}),
            {})
        with self.assertRaises(Exception):
            get_position_in_lib({'Bibliotheca Albertina': 'u8 kas iq'})

    # def test_get_book_position(self):
    #     self.assertEqual(get_book_position(get_wachtl_data('307834182')), {'Bibliotheca Albertina': 'BQ 4900 W829'})
