"""Contains utility functions used by multiple modules."""
import re
import requests
from typing import List, Dict, Any, Union, Tuple
from urllib.parse import quote
import logging
from collections import OrderedDict

from django.conf import settings
from django.utils.translation import gettext as _
from django.http import JsonResponse

Query_Response = List[Dict[str, Dict[str, str]]]
Parameters = List[Dict[str, str]]
Wachtl_Results = Dict[str, Union[str, List]]
Results = Dict[str, Dict[str, Union[str, List[str], dict]]]


def query(
        query: str,
        url: str = settings.QUERY_URL
) -> Query_Response:
    """
    Full query function that only wraps wraps request.get with minimal improvements.

    :param query: the query string
    :param url: to url to send the query to
    :return: Nested list consisting of the results part of the response
    """
    r = requests.get(url, params={"query": query})
    if r.status_code != 200:
        raise requests.RequestException(
            'error {}: {}'.format(r.status_code, r.reason))
    try:
        return r.json()['results']['bindings']
    except KeyError as e:
        raise Exception('invalid response from server: {}'.format(r.json()))


def query_simple(
        subject: str = '?s',
        predicate: str = '?p',
        object: str = '?o',
        limit: int = settings.QUERY_LIMIT,
        url: str = settings.QUERY_URL
) -> Query_Response:
    """
    Query function for simple queries using only one set of parameters.

    :param subject: subject of the query phrase
    :param predicate: predicate of the query phrase
    :param object: object of the query phrase
    :param limit: maximum number of items included in the response
    :param url: url for the query
    :return: Nested list consisting of the results part of the response
    """
    q = 'SELECT ?s ?p ?o WHERE { ' + subject + \
        ' ' + predicate + ' ' + object + ' . }'
    if limit is not None:
        q += ' LIMIT ' + str(limit)

    r = query(q, url=url)
    return r


def get_book_picture(title, isbn=None):
    """
    requires book name as a string input
    returns book picture

    :param isbn:
    :param title: str, book name
    :return: book_picture: url , book_picture
    """
    
    # always return the default url if we're testing
    if settings.TESTING:
        return settings.COVER_DEFAULT_URL

    # check if we have an isbn number
    if isbn is not None:
        return 'http://covers.openlibrary.org/b/isbn/' + isbn + '-M.jpg'

    session = requests.Session()
    wikimedia_url = 'https://commons.wikimedia.org/w/api.php?'

    # noinspection PyBroadException
    try:
        response = session.get(
                url=wikimedia_url,
                params={
                    'action': 'query',
                    'format': 'json',
                    'prop':   'images',
                    'titles': title
                }
        )
        json_data = response.json()
        page = next(iter(json_data['query']['pages'].values()))
    
        if 'images' in page:
            image_file = page['images'][0]['title']
            response_2 = session.get(
                    url=wikimedia_url,
                    params={
                        'action': 'query',
                        'format': 'json',
                        'prop':   'imageinfo',
                        'iiprop': 'url',
                        'titles': image_file
                    }
            )
            json_data_image_info = response_2.json()
            image = next(iter(json_data_image_info['query']['pages'].values()))
            return image['imageinfo'][0]['url']
    except Exception:
        pass
    
    # no suitable urls fund, use default
    return settings.COVER_DEFAULT_URL


# Dictionary holding the geographical information in this structure:
# {name:{coordinates: [int, int], street: str, city: str, name: str, variable: var}}
libraries_dict = {
    "Bibliotheca Albertina":
        {"coordinates": [51.33241, 12.36854],
         "street": "Beethovenstr. 6",
         "city": "04107 Leipzig",
         "name": "Bibliotheca Albertina",
         "variable": "1",
         "link": "https://goo.gl/maps/6rJrZgKMW8M2"},
    "Campus-Bibliothek":
        {"coordinates": [51.338497, 12.378441],
         "street": "Universitätsstr. 3 (im Hoersaalgebaeude)",
         "city": "04109 Leipzig",
         "name": "Campus-Bibliothek",
         "variable": "2",
         "link": "https://goo.gl/maps/oHhVW4VLXap"},
    "Rechtswissenschaft":
        {"coordinates": [51.337744, 12.373642],
         "street": "Burgstr. 27",
         "city": "04109 Leipzig",
         "name": "Bibliothek Rechtswissenschaften",
         "variable": "3",
         "link": "https://goo.gl/maps/1kkWueJzCrS2"},
    "Medizin/Naturwissenschaften":
        {"coordinates": [51.331768, 12.381921],
         "street": "Liebigstr. 23/25",
         "city": "04103 Leipzig",
         "name": "Bibliothek Medizin/Naturwissenschaften",
         "variable": "4",
         "link": "https://goo.gl/maps/VgAy9gm6z6t"},
    "Deutsches Literaturinstitut Leipzig":
        {"coordinates": [51.333773, 12.367756],
         "street": "Wächterstr. 34",
         "city": "04107 Leipzig",
         "name": "Bibliothek Deutsches Literaturinstitut",
         "variable": "5",
         "link": "https://goo.gl/maps/Tx8JG4aymgK2"},
    "Erziehung/Sport":
        {"coordinates": [51.33831, 12.35472],
         "street": "Marschnerstr. 29E (Haus 5). 27",
         "city": "04109 Leipzig",
         "name": "Bibliothek Erziehungs und Sportwissenschaft",
         "variable": "6",
         "link": "https://goo.gl/maps/tjjcQQ7rR8p"},
    "Archäologie":
        {"coordinates": [51.340706, 12.379565],
         "street": "Ritterstr. 14",
         "city": "04109 Leipzig",
         "name": "Bibliothek Klassische Archäologie und Ur- und Frühgeschichte",
         "variable": "6",
         "link": "https://goo.gl/maps/7BVGQoL2iy72"},
    "Kunst":
        {"coordinates": [51.341286, 12.371501],
         "street": "Dittrichring. 18-20",
         "city": "04109 Leipzig",
         "name": "Bibliothek Kunst",
         "variable": "7",
         "link": "https://goo.gl/maps/4TCN8tEXBxJ2"},
    "Musik":
        {"coordinates": [51.338459, 12.376985],
         "street": "Neumarkt 9-19 - Aufgang Städtisches Kaufhaus",
         "city": "04109 Leipzig",
         "name": "Bibliothek Musik",
         "variable": "8",
         "links": "https://goo.gl/maps/uhTwr7MXsYu"},
    "Orientwissenschaften":
        {"coordinates": [51.337201, 12.377536],
         "street": "Schillerstr. 6",
         "city": "04109 Leipzig",
         "name": "Bibliothek Orientwissenschaften",
         "variable": "9",
         "link": "https://goo.gl/maps/7Cschch9kGE2"},
    "Veterinärmedizin":
        {"coordinates": [51.320655, 12.390906],
         "street": "An den Tierkliniken 5",
         "city": "04103 Leipzig",
         "name": "Bibliothek Veterinärmedizin",
         "variable": "10",
         "link": "https://goo.gl/maps/s5p5VZeUKBy"},
    "Theaterwissenschaft":
        {"coordinates": [51.34098, 12.37962],
         "street": "Ritterstraße 16",
         "city": "04109 Leipzig",
         "name": "Nebenstandort Theaterwissenschaft",
         "variable": "11",
         "link": "https://www.google.com/maps/dir//Ritterstraße+16,+04109+Leipzig/@51.3409763,12.377526,17z/data=!4m9!4m8!1m0!1m5!1m1!1s0x47a6f823095da8c1:0xd7a12046a0d6b1a9!2m2!1d12.3797147!2d51.3409763!3e0"},
}


def get_library_information(library: str):
    """
    Return the entire library information given a library dict.

    :param library: str, name of the library
    :return: {coordinates: [int, int], street : str, city: str, name: str, variable: var}
    """
    assert isinstance(library, str)

    # TODO: add a log entry if a library is missing, make sure logging is enabled

    return libraries_dict.get(
            library,
            {
                "coordinates": None,
                "street":   _('keine Daten verfügbar'),
                "city":     _('keine Daten verfügbar'),
                "name":     _('keine Daten verfügbar'),
                "variable": "99",
                "link":     None
            }
    ).copy()


# Dictionary holding the Information where a place in the Albertina is.
albertina_dict = {
    'Maga': False,
    'OffM': [-188.5, 177],
    '1ogO': [-161, 180],
    '1ogM': [-133.5, 113.5],
    '2ogO': [-134.5, 171.5],
    '2ogM': [-105.5, 113.5],
    '2ogW': [-131, 53.5],
    '3ogO': [-98.5, 174.5],
    '3ogM': [-83, 113.5],
    '3ogW': [-97, 50]
}


def get_albertina_coordinates(room_code: str):
    """
    Take a shorthand of the room in the albertina and returns it's coordinates.

    :param room_code: str corresponding to the albertina_dict.
    :return: corresponding coordinates.
    """
    if room_code in albertina_dict:
        return albertina_dict[room_code]
    else:
        raise Exception("Couldn't match room code")


class WachtlError(Exception):
    """Defines an error, is used, if Wachtl is not working."""

    pass


def wachtl_query(url: str):
    """Return the JSON data from wachtl for a ppn."""
    try:
        # application/json doesn't work, despite what wachtl is claiming
        headers = {"Accept": "application/ld+json"}
        wachtl_response = requests.get(url, headers=headers)

        json_data = wachtl_response.json()
        return json_data
    except ValueError as e:
        raise WachtlError(e)


def extract_library_data(exemplar_data: Union[Dict[str, Any], List[Dict[str, Any]]]) -> Dict[str, Dict[str, str]]:
    """
    Return a dict with the basic data of the relevant libraries and position and availability information of the requested book.

    :param exemplar_data: the exemplar data availability part of the raw json_data from wachtl
    :return: position an relevant library information of the book
    """

    library_data = {}

    def extract_single_lib_infos(lib_data):
        """
        Extract data for a single library from the provided data. Operates directly on library_data

        :param lib_data: data on the book in a single library
        """

        # extract the library name
        library_name = lib_data['heldBy']['description']

        # ignore multiple entries for the same library
        if library_name in library_data:
            return

        # get the basic data for the library
        library_data[library_name] = get_library_information(library_name)

        # add the data from wachtl
        # TODO: use the data in 'availableFor' to provide translatable availability information?
        for key_internal, key_external in {
            'book_signature': 'label',
            'book_status': 'hasStatus'
        }.items():
            library_data[library_name][key_internal] = lib_data[key_external]
        if 'location' in lib_data:
            library_data[library_name]['book_position'] = lib_data['location']['description']

    if isinstance(exemplar_data, dict):
        # only a single data set present
        extract_single_lib_infos(exemplar_data)
    elif isinstance(exemplar_data, list):
        # list of data sets present
        for e in exemplar_data:
            if isinstance(e, dict):
                extract_single_lib_infos(e)

    return library_data


def get_book_information_from_wachtl(url: str) -> Wachtl_Results:
    """
    Get more information on the book using wachtl.

    :param url: the ppn of the book in question
    :return: libraries, availability and the link to the ubl
    """
    # this raises an exception if wachtl is not available or returns invalid data
    try:
        # raise the unavailability error if the url is not set to return the error message
        if url is None:
            raise WachtlError()

        json_data = wachtl_query(url)
    except WachtlError:
        return {
            'libraries': {},
            'errors': [_('Wachtl-Dienst nicht erreichbar. Standort- und Verfügbarkeitsinformationen sind nicht verfügbar.')],
        }

    book_details = {
        'libraries': {},
        'errors':    [],
    }
    if 'showsAvailabilityOf' in json_data:
        json_data = json_data['showsAvailabilityOf']

        book_details['link'] = json_data["url"]

        # extract position and availability data
        book_details['libraries'] = extract_library_data(json_data['exemplar'])

    else:
        book_details['errors'] = [
            _(
                    'Wachtl-Dienst lieferte keine Informationen. '
                    'Standort- und Verfügbarkeitsinformationen sind nicht vorhanden.'
            )
        ]

    return book_details


def escape_sparql(string: str) -> str:
    """
    Simple helper to escape a string to allow for searches with reserved characters and to avoid injection attacks.

    :param string: the string that needs to be escaped
    :return: the escaped version of the input string
    """
    return string.replace('"', '\"')


def generate_query_parts_from_params(parameters: Parameters) -> Dict[str, Union[str, int]]:
    """
    Build a SPARQL query from a set of parameters given as a list of key value pairs. Unknown keys will be ignored.

    :param parameters: the dict containing the parameters
    :return: the variable parts of SPARQL query
    """

    where = ''
    sort = 'ORDER BY ?id'
    offset = 0

    for param in parameters:
        # search filters
        if 'string' in param:
            where += '?id schema:name ?title. FILTER CONTAINS( LCASE(?title), "{}" ). '.format(
                    param['string'].lower()
            )
        elif 'title' in param:
            where += '?id schema:name "{}". '.format(
                    param['title']
            )
        elif 'rvk' in param:
            where += '?id schema:about <https://rvk.uni-regensburg.de/api/json/ancestors/{}>. '.format(
                param['rvk'].replace(' ', '+')
            )
        elif 'isbn' in param:
            where += '?id schema:isbn {}. '.format(param['isbn'])
        elif 'issn' in param:
            where += '?id schema:issn {}. '.format(param['issn'])
        elif 'author' in param:
            where += '?id schema:author/schema:name "{}". '.format(param['author'])
        elif 'id' in param:
            where += 'BIND(<http://data.slub-dresden.de/resources/{}> AS ?id) '.format(param['id'])
        elif 'language' in param:
            where += '?id schema:inLanguage "{}". '.format(param['language'])

        # sort directive (only the last one is used)
        # we can only sort by attributes with only one value or we would mess up the ordering of the results
        # contrary to the above is sort by language still allowed but some titles may appear twice if used
        elif 'sort' in param:
            # check if is valid
            m = re.match(r'(?P<attribute>\w+)-(?P<order>(?:asc)|(?:desc))', param['sort'])
            if m is not None:
                attributes = {'title': '?title', 'author': '?author', 'language': '?language'}
                scheme = attributes.get(m.group('attribute'), None)

                if scheme is not None:
                    sort = 'ORDER BY {}({})'.format(m.group('order').upper(), scheme)

        # as with sort only the last page directive will be used
        elif 'page' in param:
            offset = (int(param['page']) - 1) * settings.ITEMS_PER_PAGE  # page numbers start at 1
            if offset < 0:
                offset = 0

    # stuff that needs to be done after converting the parameters to query parts
    where = escape_sparql(where)

    return {'where': where, 'sort': sort, 'offset': offset}


def extract_data_from_response(response: Query_Response) -> Tuple[Results, bool]:
    """
    Extract usable data from the server response. Collects all data regarding a title into a single data set.

    :param response: the raw response from the server as returned by our own query() helper
    :return: the results and a hint if should add a link ot the next page
    """

    # TODO: can we find infos on: edition of the book?

    # dictionaries to map the property name to the internal name
    # separated into items that are only allowed to occur once and items that may occur multiple times
    items_single = {
        'http://schema.org/name': 'name',
        'http://schema.org/offers': 'wachtl_url',
        'http://schema.org/isbn': 'isbn',
        'http://schema.org/issn': 'issn',
        'http://schema.org/datePublished': 'datePublished',
        'http://schema.org/publisher': 'publisher'
    }
    items_multi = {
        'http://schema.org/author': 'authors',
        'http://schema.org/inLanguage': 'languages',
        'http://schema.org/about': 'rvk_codes',
    }

    results = OrderedDict()  # use OrderedDict because dict only remembers the insertion order in python3.7 or later
    next_page = False
    for data_set in response:
        # extract the id
        # underscore at the end of the variable name to avoid shadowing the builtin function with the same name
        id_ = data_set['id']['value']

        # check if we've encountered data for this title before
        # , add a template if not or set the flag to display the pagination
        if id_ not in results:
            if len(results) >= settings.ITEMS_PER_PAGE:
                next_page = True
                break

            # items with only a single value allowed aren't in the template to allow checking for their existence
            results[id_] = {
                'authors':    [],
                'languages':  [],
                'rvk_codes':  [],
                'libraries':  {},
                'errors':     [],
            }

        # skip this data set if we got a bnode as result
        if data_set['value']['type'] == 'bnode':
            continue

        property_name = data_set['property']['value']
        # check if we find data for items that are only allowed to occur once
        if property_name in items_single:
            if items_single[property_name] not in results[id_]:
                results[id_][items_single[property_name]] = data_set['value']['value']
        # add data for items with multiple allowed values if the value is not already known
        elif property_name in items_multi:
            value = data_set['value']['value']
            if value not in results[id_][items_multi[property_name]]:
                results[id_][items_multi[property_name]].append(value)

    # do some postprocessing
    ret = OrderedDict()
    for id_, data in results.items():
        # shorten and prettify the rvk codes
        codes = []
        for c in data['rvk_codes']:
            m = re.match(r'https://rvk.uni-regensburg.de/api/json/ancestors/(\w+)\+(\d+)', c)
            if m is not None:
                codes.append('{} {}'.format(
                        m.group(1),
                        m.group(2)
                ))
        data['rvk_codes'] = codes

        # extract the ppn from the wachtl url if we have one and we didn't extract the ppn earlier
        if 'wachtl_url' in data and 'ppn' not in data:
            m = re.match(r'{}(\w+)'.format(settings.WACHTL_URL), data['wachtl_url'])
            if m is not None:
                data['ppn'] = m.group(1)

        # try to shorten the id
        m = re.match(r'http://data.slub-dresden.de/resources/(\w+)', id_)
        if m is not None:
            # id matched the pattern, save the shortened form else ignore the dataset
            ret[m.group(1)] = data

    return ret, next_page


def build_query_string(parameters: Parameters) -> str:
    """
    Builds a SPARQL query string from a set of parameters extracted from the url.

    :param parameters: list of key-value pairs wit the parameters
    :return: the complete quer string
    """

    query_parts = generate_query_parts_from_params(parameters)

    # generate additional conditions when sorting is active
    # only one sort directive at a time is supported
    m = re.match(r'ORDER BY (?:(?:ASC)|(?:DESC))\(\?(\w+)\)', query_parts['sort'])
    sort_select = ''
    sort_where = ''
    if m is not None:
        if m.group(1) == 'author':
            sort_select = '(GROUP_CONCAT(?author_ ; SEPARATOR="; ") AS ?author)'
            sort_where = 'OPTIONAL { ?id schema:author/schema:name ?author_. }'
        elif m.group(1) == 'language':
            sort_select = '(GROUP_CONCAT(?language_ ; SEPARATOR="; ") AS ?language)'
            sort_where = 'OPTIONAL { ?id schema:inLanguage ?language_. }'

    query_string = 'PREFIX schema:<http://schema.org/> '
    query_string += 'SELECT ?id ?property '
    query_string += '(COALESCE(?val2, ?val1) as ?value) WHERE {'
    query_string += '{'
    # first subquery to generate a list of IDs used when requesting the properties for each title.
    # concatenate author and language information to ensure a single data set per title even
    # with multiple authors/languages. We use grouping instead of DISTINCT for this reason
    query_string += '  SELECT ?id'
    query_string += '  {}'.format(sort_select)
    query_string += '  WHERE {'
    query_string += '    {}'.format(query_parts['where'])
    query_string += '    ?id a schema:CreativeWork.'
    query_string += '    ?id schema:name ?title.'
    # this line ensures we have availability data
    query_string += '    ?id schema:offers/schema:offers/schema:availability ?availability.'
    query_string += '    {}'.format(sort_where)
    query_string += '  }'
    # use grouping to avoid having any ID more than once
    query_string += '  GROUP BY ?id'
    # first sort directive to get the correct ordering when we have multiple pages of results
    query_string += '  {}'.format(query_parts['sort'])
    # second part of the query: get all relevant attributes for every ID
    query_string += '  LIMIT {} OFFSET {}'.format(settings.ITEMS_PER_PAGE + 1, query_parts['offset'])
    query_string += '  }'
    query_string += '  {'
    # use a list of specific property names for the requests
    query_string += '    ?id ?property ?val1.'
    query_string += '    VALUES ?property {'
    query_string += '      schema:name schema:inLanguage schema:about schema:offers schema:author '
    query_string += '      schema:isbn schema:issn schema:datePublished' 'schema:publisher'
    query_string += '    }'
    query_string += '  ?id schema:name ?title.'
    # some properties require a SPARQL path to access usable data. those are requested here in conjunction with COALESCE
    query_string += '  OPTIONAL {'
    query_string += '    ?val1 schema:name ?val2.'
    query_string += '    FILTER NOT EXISTS {?val1 schema:keywords ?val2}'
    query_string += '  }'
    query_string += '  OPTIONAL { ?val1 schema:offers/schema:availability ?val2. }'
    query_string += '  }'
    query_string += '} '
    # second sort directive to actually sort the data sets we've got so we're able to display them in the right order
    query_string += '{}'.format(query_parts['sort'])

    return query_string


def load_book_information(parameters: Parameters) -> Dict[str, Union[Results, bool]]:
    """
    Load information on the requested titles from various sources.

    :param parameters: A set of parameters usually extracted from the url
    :return: a dict with all the available information on each of the requested titles
    """

    # build and execute query
    query_string = build_query_string(parameters)
    response = query(query_string)

    results, next_page = extract_data_from_response(response)

    # load and insert data from wachtl
    for id_, book_details in results.items():
        wachtl_data = get_book_information_from_wachtl(book_details.get('wachtl_url', None))
        book_details['libraries'].update(wachtl_data['libraries'])
        book_details['errors'] += wachtl_data['errors']
        if 'link' in wachtl_data:
            book_details['link'] = wachtl_data['link']

        # collect availability data
        states = []
        for lib_name, lib_data in book_details['libraries'].items():
            if lib_data['book_status'] not in states:
                states.append(lib_data['book_status'])
            book_details['availability'] = '; '.join(states)

        # get cover image and add the image to book_details
        isbn = None
        if 'isbn' in book_details:
            isbn = book_details['isbn']
        book_details['cover_image'] = get_book_picture(title=book_details['name'], isbn=isbn)

    return {'results': results, 'next_page': next_page}
