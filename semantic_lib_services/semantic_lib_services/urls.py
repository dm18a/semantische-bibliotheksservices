"""semantic_lib_services URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.urls import include, path, re_path
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView

urlpatterns = [
    path('impressum', TemplateView.as_view(template_name='impressum.html')),
    path('kontakt', TemplateView.as_view(template_name='kontakt.html')),
    path('help', TemplateView.as_view(template_name='help.html')),
    re_path(r'(?:^search/)|(?:^$)', include('search.urls')),
    path('title/', include('title.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    path('mock_api/', include('mock_api.urls'))
]

urlpatterns += i18n_patterns(
    path('admin/', admin.site.urls),
    path('impressum', TemplateView.as_view(template_name='impressum.html')),
    path('kontakt', TemplateView.as_view(template_name='kontakt.html')),
    path('help', TemplateView.as_view(template_name='help.html')),
    re_path(r'(?:^search/)|(?:^$)', include('search.urls')),
    path('title/', include('title.urls')),
)

# serve static asses through django only in debug mode
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
