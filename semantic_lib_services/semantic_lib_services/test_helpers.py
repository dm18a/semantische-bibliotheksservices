from collections import OrderedDict

from django.conf import settings
from django.test import TestCase
from semantic_lib_services.helpers import get_book_information_from_wachtl, get_library_information, \
    get_albertina_coordinates, extract_library_data, generate_query_parts_from_params, wachtl_query, \
    extract_data_from_response, load_book_information


class TestHelpers(TestCase):
    def test_get_library_information(self):
        self.assertEqual(
                get_library_information('Bibliotheca Albertina'),
                {
                    "coordinates": [51.33241, 12.36854],
                    "street":   "Beethovenstr. 6",
                    "city":     "04107 Leipzig",
                    "name":     "Bibliotheca Albertina",
                    "variable": "1",
                    "link":     "https://goo.gl/maps/6rJrZgKMW8M2"
                }
        )
        self.assertEqual(get_library_information("Keine echte Bibliothek"),
                         {
                             "coordinates": None,
                             "street": ('keine Daten verfügbar'),
                             "city": ('keine Daten verfügbar'),
                             "name": ('keine Daten verfügbar'),
                             "variable": "99",
                             "link": None
                         })
        self.assertEqual(get_library_information(""),
                         {
                             "coordinates": None,
                             "street": ('keine Daten verfügbar'),
                             "city": ('keine Daten verfügbar'),
                             "name": ('keine Daten verfügbar'),
                             "variable": "99",
                             "link": None
                         })
        with self.assertRaises(AssertionError):
            get_library_information(123)

    def test_get_albertina_coordinates(self):
        self.assertEqual(get_albertina_coordinates('OffM'), [-188.5, 177])
        with self.assertRaises(Exception):
            get_albertina_coordinates('Fehlerhafter Raumcode')

    def test_extract_library_data(self):
        self.assertEqual(extract_library_data({
            "@id": "DE-15:ppn:307834182:0022053734",
            "@type": "Item",
            "hasStatus": "ausleihbar",
            "availableFor": [
                {
                    "@type": "Presentation"
                },
                {
                    "@type": "Loan"
                }
            ],
            "heldBy": {
                "@id": "http://data.ub.uni-leipzig.de/resource/DE-15/department/zw01",
                "@type": "Organization",
                "description": "Bibliotheca Albertina"
            },
            "label": "BQ 4900 W829",
            "url": "https://katalog.ub.uni-leipzig.de/Record/0007894383",
            "location": {
                "@type": "Storage",
                "description": "Freihandbereich Theologie"
            }
        }), {"Bibliotheca Albertina": {
            "coordinates": [
                51.33241,
                12.36854
            ],
            "street": "Beethovenstr. 6",
            "city": "04107 Leipzig",
            "name": "Bibliotheca Albertina",
            "variable": "1",
            "link": "https://goo.gl/maps/6rJrZgKMW8M2",
            "book_signature": "BQ 4900 W829",
            "book_status": "ausleihbar",
            "book_position": "Freihandbereich Theologie"
        }
        })

        with self.assertRaises(KeyError):
            extract_library_data({'No Data': 'No Data'})

        self.assertEqual(extract_library_data(None), {})
        self.assertEqual(extract_library_data('a string'), {})
        self.assertEqual(extract_library_data(123), {})
        self.assertEqual(extract_library_data([
            {
                "@id": "DE-15:ppn:325701180:0014429314",
                "@type": "Item",
                "hasStatus": "Pr\u00e4senz",
                "availableFor": {
                    "@type": "Presentation"
                },
                "heldBy": {
                    "@id": "http://data.ub.uni-leipzig.de/resource/DE-15/department/zw01",
                    "@type": "Organization",
                    "description": "Bibliotheca Albertina"
                },
                "label": "BQ 4900 K66",
                "url": "https://katalog.ub.uni-leipzig.de/Record/0000887825",
                "location": {
                    "@type": "Storage",
                    "description": "Freihandbereich Theologie"
                }
            },
            {
                "@id": "DE-15:ppn:325701180:0018328347",
                "@type": "Item",
                "hasStatus": "ausgeliehen, Vormerkung m\u00f6glich",
                "heldBy": {
                    "@id": "http://data.ub.uni-leipzig.de/resource/DE-15/department/zw11",
                    "@type": "Organization",
                    "description": "Musik"
                },
                "label": "LR 32000 K66",
                "unavailableFor": [
                    {
                        "@type": "Presentation",
                        "expected": "2019-04-04T00:00:00.000+02:00"
                    },
                    {
                        "@type": "Loan",
                        "expected": "2019-04-04T00:00:00.000+02:00",
                        "limitatedBy": {
                            "@id": "DE-15:ReservationPossible",
                            "@type": "Limitation",
                            "description": "Vormerkung m\u00f6glich"
                        },
                        "url": "https://katalog.ub.uni-leipzig.de/MyResearch"
                    }
                ],
                "url": "https://katalog.ub.uni-leipzig.de/Record/0000887825",
                "location": {
                    "@type": "Storage",
                    "description": "Freihandbereich"
                }
            }
        ]), {
            "Bibliotheca Albertina": {
                "coordinates": [
                    51.33241,
                    12.36854
                ],
                "street": "Beethovenstr. 6",
                "city": "04107 Leipzig",
                "name": "Bibliotheca Albertina",
                "variable": "1",
                "link": "https://goo.gl/maps/6rJrZgKMW8M2",
                "book_signature": "BQ 4900 K66",
                "book_status": "Präsenz",
                "book_position": "Freihandbereich Theologie"
            },
            "Musik": {
                "coordinates": [
                    51.338459,
                    12.376985
                ],
                "street": "Neumarkt 9-19 - Aufgang Städtisches Kaufhaus",
                "city": "04109 Leipzig",
                "name": "Bibliothek Musik",
                "variable": "8",
                "links": "https://goo.gl/maps/uhTwr7MXsYu",
                "book_signature": "LR 32000 K66",
                "book_status": "ausgeliehen, Vormerkung möglich",
                "book_position": "Freihandbereich"
            }
        })

    def test_build_query_parts_from_params(self):
        self.assertEqual(generate_query_parts_from_params([]), {'where': '', 'sort': 'ORDER BY ?id', 'offset': 0})
        self.assertEqual(generate_query_parts_from_params('abc'), {'where': '', 'sort': 'ORDER BY ?id', 'offset': 0})
        self.assertEqual(generate_query_parts_from_params({}), {'where': '', 'sort': 'ORDER BY ?id', 'offset': 0})
        self.assertEqual(generate_query_parts_from_params([{'author': 'Segebrecht, Wulf'}, {'sort': 'title-desc'}]),
                         {'where': '?id schema:author/schema:name "Segebrecht, Wulf". ',
                          'sort': 'ORDER BY DESC(?title)', 'offset': 0})
        self.assertEqual(generate_query_parts_from_params([{'rvk': 'BQ 4900'}, {'sort': 'title-desc'}, {'page': 2}]),
                         {'where': '?id schema:about <https://rvk.uni-regensburg.de/api/json/ancestors/BQ+4900>. ',
                          'sort': 'ORDER BY DESC(?title)', 'offset': 20})
        self.assertEqual(generate_query_parts_from_params([{'id': 'D43302CCA54A53EE'}]),
                         {'offset': 0,
                          'sort': 'ORDER BY ?id',
                          'where': 'BIND(<http://data.slub-dresden.de/resources/D43302CCA54A53EE> AS ?id) '})
        with self.assertRaises(TypeError):
            generate_query_parts_from_params(None)
        with self.assertRaises(TypeError):
            generate_query_parts_from_params(123)
        with self.assertRaises(TypeError):
            generate_query_parts_from_params({'rvk': 'GE 5517'})
            
    def test_extract_data_from_response(self):
        self.assertEqual(
                extract_data_from_response([]),
                (OrderedDict(), False)
        )
        self.assertEqual(
                extract_data_from_response([{
                    "id":       {"type": "uri", "value": "http://data.slub-dresden.de/resources/TESTBOOK0"},
                    "property": {"type": "uri", "value": "http://schema.org/name"},
                    "value":    {"type": "literal", "value": "Test Book 0"}
                }, ]),
                (
                    OrderedDict({'TESTBOOK0': {
                        'name':      'Test Book 0',
                        'authors':   [],
                        'languages': [],
                        'rvk_codes': [],
                        'libraries': {},
                        'errors':    [],
                    }}),
                    False
                )
        )
        self.assertEqual(
                extract_data_from_response([
                    {
                        "id":       {"type": "uri", "value": "http://data.slub-dresden.de/resources/TESTBOOK1"},
                        "property": {"type": "uri", "value": "http://schema.org/name"},
                        "value":    {"type": "literal", "value": "Test Book 1"}
                    },
                    {
                        "id":       {"type": "uri", "value": "http://data.slub-dresden.de/resources/TESTBOOK1"},
                        "property": {"type": "uri", "value": "http://schema.org/publisher"},
                        "value":    {"type": "literal", "value": "Test Publisher 1"}
                    },
                    {
                        "id":       {"type": "uri", "value": "http://data.slub-dresden.de/resources/TESTBOOK1"},
                        "property": {"type": "uri", "value": "http://schema.org/author"},
                        "value":    {"type": "literal", "value": "Test Author 1.0"}
                    },
                    {
                        "id":       {"type": "uri", "value": "http://data.slub-dresden.de/resources/TESTBOOK1"},
                        "property": {"type": "uri", "value": "http://schema.org/author"},
                        "value":    {"type": "literal", "value": "Test Author 1.1"}
                    },
                    {
                        "id":       {"type": "uri", "value": "http://data.slub-dresden.de/resources/TESTBOOK1"},
                        "property": {"type": "uri", "value": "http://schema.org/inLanguage"},
                        "value":    {"type": "literal", "value": "ger"}
                    },
                ]),
                (
                    OrderedDict({'TESTBOOK1': {
                        'name':      'Test Book 1',
                        'publisher': 'Test Publisher 1',
                        'authors':   ['Test Author 1.0', 'Test Author 1.1'],
                        'languages': ['ger'],
                        'rvk_codes': [],
                        'libraries': {},
                        'errors':    [],
                    }}),
                    False
                )
        )
    
        request = []
        result = OrderedDict()
        for i in range(settings.ITEMS_PER_PAGE + 1):
            request.append(
                        {
                            "id":       {"type": "uri", "value": "http://data.slub-dresden.de/resources/TESTBOOK{}".format(i)},
                            "property": {"type": "uri", "value": "http://schema.org/name"},
                            "value":    {"type": "literal", "value": "Test Book {}".format(i)}
                        }
            )
            
            if i >= settings.ITEMS_PER_PAGE:
                continue
            
            result['TESTBOOK{}'.format(i)] = {
                'name':      'Test Book {}'.format(i),
                'authors':   [],
                'languages': [],
                'rvk_codes': [],
                'libraries': {},
                'errors':    [],
            }
        self.assertEqual(extract_data_from_response(request), (result, True))
        
    def test_load_book_information(self):
        # construct the expected answer
        library_data = {'Bibliotheca Albertina': {
            'coordinates':    [51.33241, 12.36854],
            'street':         'Beethovenstr. 6',
            'city':           '04107 Leipzig',
            'name':           'Bibliotheca Albertina',
            'variable':       '1',
            'link':           'https://goo.gl/maps/6rJrZgKMW8M2',
            'book_signature': '97-8-23613',
            'book_status':    'ausleihbar',
            'book_position':  'Offenes Magazin'
        }}
        result = OrderedDict()
        for i in range(settings.TEST_ITEMS_AMOUNT):
            result['TESTBOOK{}'.format(i)] = {
                'authors':      ['Test Author {}'.format(i)],
                'languages':    ['ger'],
                'rvk_codes':    ['GE 5517'],
                'libraries':    library_data,
                'errors':       [],
                'wachtl_url':   'http://localhost:8099/mock_api/wachtl/ppn/valid_ppn',
                'name':         'Test Book {}'.format(i),
                'ppn':          'valid_ppn',
                'link':         'https://katalog.ub.uni-leipzig.de/Record/0007894383',
                'availability': 'ausleihbar',
                'cover_image': settings.COVER_DEFAULT_URL,
            }
        self.assertEqual(
                load_book_information([{'rvk': 'GE 5517'}]),
                {'next_page': False, 'results': result}
        )
        
        self.assertEqual(
                load_book_information([{'id': 'TESTBOOK0'}]),
                {
                    'next_page': False,
                    'results':   OrderedDict({
                        'TESTBOOK0': {
                            'authors':      ['Test Author 0'],
                            'languages':    ['ger'],
                            'rvk_codes':    ['GE 5517'],
                            'libraries':    {
                                'Bibliotheca Albertina': {
                                    'coordinates':    [51.33241, 12.36854],
                                    'street':         'Beethovenstr. 6',
                                    'city':           '04107 Leipzig',
                                    'name':           'Bibliotheca Albertina',
                                    'variable':       '1',
                                    'link':           'https://goo.gl/maps/6rJrZgKMW8M2',
                                    'book_signature': '97-8-23613',
                                    'book_status':    'ausleihbar',
                                    'book_position':  'Offenes Magazin'
                                }
                            },
                            'errors':       [],
                            'wachtl_url':   'http://localhost:8099/mock_api/wachtl/ppn/valid_ppn',
                            'name':         'Test Book 0',
                            'ppn':          'valid_ppn',
                            'link':         'https://katalog.ub.uni-leipzig.de/Record/0007894383',
                            'availability': 'ausleihbar',
                            'cover_image':  settings.COVER_DEFAULT_URL,
                        }
                    })
                }
        )


class TestWachtl(TestCase):
    def test_wachtl_query(self):
        self.assertEqual(
                wachtl_query(settings.WACHTL_URL + 'valid_ppn'),
                {
                    "@context":            {},
                    "@type":               "Response",
                    "showsAvailabilityOf": {
                        "@id":      "DE-15:ppn:060035129",
                        "@type":    "Document",
                        "exemplar": {
                            "@type":        "Item",
                            "hasStatus":    "ausleihbar",
                            "availableFor": [{"@type": "Presentation"}, {"@type": "Loan"}],
                            "heldBy":       {
                                "@id":   "http://data.ub.uni-leipzig.de/resource/DE-15/department/zw01",
                                "@type": "Organization", "description": "Bibliotheca Albertina"
                            },
                            "label":        "97-8-23613",
                            "location":     {"@type": "Storage", "description": "Offenes Magazin"}
                        },
                        "url":      "https://katalog.ub.uni-leipzig.de/Record/0007894383"
                    }
                }
        )
        self.assertEqual(
                wachtl_query(settings.WACHTL_URL + 'not_a_valid_ppn'),
                {
                    "@context":            {},
                    "@type":               "Response",
                }
        )

    def test_get_book_information_from_wachtl(self):
        self.assertEqual(
                get_book_information_from_wachtl(settings.WACHTL_URL + 'valid_ppn'),
                {
                    "libraries": {
                        'Bibliotheca Albertina': {
                            'book_position':  'Offenes Magazin',
                            'book_signature': '97-8-23613',
                            'book_status':    'ausleihbar',
                            'city':           '04107 Leipzig',
                            'coordinates':    [51.33241, 12.36854],
                            'link':           'https://goo.gl/maps/6rJrZgKMW8M2',
                            'name':           'Bibliotheca Albertina',
                            'street':         'Beethovenstr. 6',
                            'variable':       '1'
                        }
                    },
                    "errors":    [],
                    "link":      "https://katalog.ub.uni-leipzig.de/Record/0007894383"
                }
        )
        self.assertEqual(
                get_book_information_from_wachtl("{}notavalidppn".format(settings.WACHTL_URL)),
                {'libraries': {},
                    'errors': [(
                        'Wachtl-Dienst lieferte keine Informationen. '
                        'Standort- und Verfügbarkeitsinformationen sind nicht vorhanden.')], })
        self.assertEqual(get_book_information_from_wachtl(""),
                {'libraries': {},
                    'errors': [(
                        'Wachtl-Dienst nicht erreichbar. Standort- und Verfügbarkeitsinformationen sind nicht verfügbar.')], })
        self.assertEqual(get_book_information_from_wachtl(None),
                {'libraries': {},
                    'errors': [(
                        'Wachtl-Dienst nicht erreichbar. Standort- und Verfügbarkeitsinformationen sind nicht verfügbar.')], })
        self.assertEqual(get_book_information_from_wachtl(123),
                {'libraries': {},
                    'errors': [(
                        'Wachtl-Dienst nicht erreichbar. Standort- und Verfügbarkeitsinformationen sind nicht verfügbar.')], })
