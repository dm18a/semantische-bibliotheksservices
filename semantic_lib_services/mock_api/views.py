import logging
from django.http.response import JsonResponse, Http404
from django.views.decorators.http import require_GET
from django.conf import settings

from semantic_lib_services.helpers import build_query_string


@require_GET
def fuseki(request):
    """ Mock-up for the fuseki server from the UBL. """
    
    assert 'query' in request.GET
    query = request.GET['query']
    
    def generate_book_info(number: int):
        """
        Helper function to generate a list with a set of fake book information to the bindings list.
        
        :param number: the number of the book we want to fake
        """
        
        book_info = []
        id_ = 'http://data.slub-dresden.de/resources/TESTBOOK{}'.format(number)
        properties_uri = {
            'http://schema.org/about':  'https://rvk.uni-regensburg.de/api/json/ancestors/GE+5517',
            'http://schema.org/offers': '{}valid_ppn'.format(settings.WACHTL_URL),
        }
        properties_literal = {
            'http://schema.org/name':       'Test Book {}'.format(number),
            'http://schema.org/inLanguage': 'ger',
            'http://schema.org/author':     'Test Author {}'.format(number),
        }
    
        for property_, value in properties_uri.items():
            book_info.append({
                "id":       {'type': 'uri', 'value': id_},
                "property": {'type': 'uri', 'value': property_},
                "value":    {'type': 'uri', 'value': value},
            })
        for property_, value in properties_literal.items():
            book_info.append({
                "id":       {'type': 'uri', 'value': id_},
                "property": {'type': 'uri', 'value': property_},
                "value":    {'type': 'literal', 'value': value},
            })
        
        return book_info
    
    # list to save the bindings and a dict for all of the responses to certain queries
    bindings = []
    responses = {}
    
    for i in range(settings.TEST_ITEMS_AMOUNT):
        # append to bindings list for the search query
        bindings += generate_book_info(i)
        
        # add response for queries for a single title
        responses[build_query_string([{'id': 'TESTBOOK{}'.format(i)}])] = {
            "head": {"vars": ["id", "property", "value"]},
            "results": {"bindings": generate_book_info(i)}
        }
    
    # build and add the response for the search request (query="rvk:"GE 5517", no sort directive)
    responses[build_query_string([{'rvk': 'GE 5517'}])] = {
        "head": {"vars": ["id", "property", "value"]},
        "results": {"bindings": bindings}
    }
    
    if query in responses:
        return JsonResponse(responses[query])
    else:
        message = {'error': 'unknown query: {}'.format(query), 'known queries': list(responses.keys())}
        logging.log(logging.CRITICAL, message)
        return JsonResponse(message)


@require_GET
def wachtl(request, ppn):
    """
    Mock-up for the wachtl server from the UBL.
    Answers contain only parts that will be evaluated by the program.
    """
    
    if ppn == 'valid_ppn':
        response = {
            "@context":            {},
            "@type":               "Response",
            "showsAvailabilityOf": {
                "@id":      "DE-15:ppn:060035129",
                "@type":    "Document",
                "exemplar": {
                    "@type":        "Item",
                    "hasStatus":    "ausleihbar",
                    "availableFor": [{"@type": "Presentation"}, {"@type": "Loan"}],
                    "heldBy":       {
                        "@id":         "http://data.ub.uni-leipzig.de/resource/DE-15/department/zw01",
                        "@type":       "Organization",
                        "description": "Bibliotheca Albertina"
                    },
                    "label":        "97-8-23613",
                    "location":     {"@type": "Storage", "description": "Offenes Magazin"}
                },
                "url":      "https://katalog.ub.uni-leipzig.de/Record/0007894383"
            }
        }
        return JsonResponse(response)
    else:
        response = {"@context": {}, "@type": "Response"}
        return JsonResponse(response, safe=False)
