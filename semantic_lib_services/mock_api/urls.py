from django.urls import re_path, path

from .views import fuseki, wachtl

urlpatterns = [
    path('fuseki', fuseki),
    path('wachtl/ppn/<str:ppn>', wachtl)
]
