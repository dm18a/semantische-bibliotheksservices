"""This modules handles search requests and renders their results."""
import re
import string
from urllib.parse import quote, unquote
from typing import Dict, List

from django.conf import settings
from django.shortcuts import render, redirect
from django.views.decorators.http import require_POST
from django.http import JsonResponse
from django.utils.translation import gettext as _
import requests
import datetime

from semantic_lib_services.helpers import query, query_simple, load_book_information


Parameters = List[Dict[str, str]]


def receive_search_query(request):
    """
    Receives the entered query string and converts it to a REST compatible url.

    :param request: the query string as entered in the form on the website
    :return: a redirect to the cleaner url
    """
    # TODO: write a couple of tests for this

    query_string = request.POST.get('query', '')

    # redirect to the last page on invalid request methods and on empty queries
    if request.method != 'POST' or query_string == '':
        return redirect(request.POST.get('last_path', '/'))

    # the regex matches single words, strings in quote marks with spaces and both with "prefix:" in front of it
    regex = r'(?P<main>' \
            r'(?:(?:\w+:)?(?P<quote_mark>["\']).+?(?P=quote_mark))' \
            r'|(?:(?:\w+:)?\w+)' \
            r')'

    # get all matches with the regex and add them as parameters ot the url
    matches = re.finditer(regex, query_string)
    url = ''
    for match in matches:
        # get the string from the match object
        p = match.group('main')

        # check if it is a named argument
        m = re.match(r'(?P<name>\w+):(?P<arg>(?P<quote_mark>["\']).+?(?P=quote_mark))', p)

        def remove_quote_marks(s: str) -> str:
            """
            Tiny helper to remove quote marks from arguments if present.

            :param s: argument string
            :return: unquoted argument string
            """
            if (s[0] == "'" or s[0] == '"') and s[0] == s[-1]:
                s = s[1:-1]
            return s

        if m is None:  # not a named argument
            url += '/string/' + remove_quote_marks(p)
        else:  # is a named argument
            name = m.group('name')
            arg = remove_quote_marks(m.group('arg'))

            url += '/' + name + '/' + arg

    # escape url
    url = quote(url)

    # redirect to the search url
    return redirect('/search' + url)


def extract_params_from_url(url_remainder: str) -> Parameters:
    """
    Convert a url remainder to a list of key value pairs.

    :param url_remainder: what remains of the url after the routing in django.
    :return: dict with key value pairs
    """

    parts = url_remainder.split('/')
    params = []
    for i in range(1, len(parts), 2):
        params += [{parts[i - 1]: parts[i]}]
    return params


def generate_pagination(params: Parameters, next_page: bool) -> List[Dict[str, str]]:
    """
    Generate a list with the text and link targets for the pagination buttons.

    :param params: the params list as extracted by extract_params_from_url()
    :param next_page: display a button to the next page?
    :return: list with pagination buttons
    """
    # generate the basic url and extract the current page
    current_page = 1
    base_url = '/search'
    for p in params:
        if 'page' in p:
            current_page = int(p['page']) if int(p['page']) >= 1 else 1
        else:
            items = list(p.items())
            base_url += '/{}/{}'.format(items[0][0], items[0][1])

    # we're on the first page and don't have a next one: don't show pagination
    if current_page == 1 and not next_page:
        return []

    # generate the text and link target for the buttons of the pagination
    pagination = [{'text': _('erste Seite'), 'target': base_url, 'active': current_page == 1}]
    for i in range(1, current_page + 1):
        target = base_url if i == 1 else base_url + '/page/' + str(i)
        pagination.append({'text': str(i), 'target': target, 'active': i == current_page})
    if next_page:
        pagination.append(
                {'text': _('nächste Seite'), 'target': base_url + '/page/' + str(current_page + 1), 'active': False}
        )

    return pagination


def get_author_information(author_name: str):
    """
    Looks up given author in wikidata and gets a few pieces of information.
    :param author_name: The full name of the author.
    :return: dictionary of information about the author.
    """
    url = 'https://query.wikidata.org/sparql'
    # author_name should be FIRST NAME LAST NAME, but is usually LAST NAME, FIRST NAME
    if ',' in author_name:
        author_last_name = author_name.split(',')[0]
        author_first_name = author_name.split(',')[1][1:]
        author_name = author_first_name + ' ' + author_last_name

    author_id_query = """
    SELECT DISTINCT
    ?author_id
    WHERE {{
    ?author_id ?label "{author}".
    ?article schema:about ?author_id. }}
    """.format(author=author_name)

    author_id_request = requests.get(url, params={'format': 'json', 'query': author_id_query})
    # check for answer
    if (author_id_request.status_code == 200 and len(author_id_request.json()['results']['bindings']) != 0):
        author_url = author_id_request.json()['results']['bindings'][0]['author_id']['value']
        author_id = author_url[31:]

        author_info_query = """
        SELECT
        ?Geburtsdatum ?GeburtsortLabel ?Land_der_StaatsangehoerigkeitLabel ?Sterbedatum ?SterbeortLabel
        WHERE {{
        OPTIONAL {{ wd:{author} wdt:P569 ?Geburtsdatum. }}
        OPTIONAL {{ wd:{author} wdt:P19 ?Geburtsort. }}
        OPTIONAL {{ wd:{author} wdt:P27 ?Land_der_Staatsangehoerigkeit. }}
        OPTIONAL {{ wd:{author} wdt:P570 ?Sterbedatum. }}
        OPTIONAL {{ wd:{author} wdt:P20 ?Sterbeort. }}
        SERVICE wikibase:label {{ bd:serviceParam wikibase:language "de". }}}}
        """.format(author=author_id)
        print(author_info_query)

        author_info_request = requests.get(url, params={'format': 'json', 'query': author_info_query})
        print(author_info_request)
        author_info = {}
        if (author_info_request.status_code == 200 and len(author_info_request.json()['results']['bindings']) != 0):
            author_info_json = author_info_request.json()['results']['bindings'][0]
            author_info['name'] = author_name
            for attribute, values in author_info_json.items():
                if (attribute == 'Geburtsdatum'):
                    author_info['date_of_birth'] = datetime.datetime.strptime(values['value'][:10], '%Y-%m-%d').strftime('%d.%m.%Y')

                elif (attribute == 'Sterbedatum'):
                    author_info['date_of_death'] = datetime.datetime.strptime(values['value'][:10], '%Y-%m-%d').strftime('%d.%m.%Y')

                elif (attribute == 'GeburtsortLabel'):
                    author_info['place_of_birth'] = values['value']

                elif (attribute == 'SterbeortLabel'):
                    author_info['place_of_death'] = values['value']

                elif (attribute == 'Land_der_StaatsangehoerigkeitLabel'):
                    author_info['nation'] = values['value']

        else:
            # if no info found, don't return any.
            author_info = False

        return author_info
    

def generate_query_string_from_params(params: Parameters) -> str:
    """
    Reconstruct the entered query string based on the extracted parameters
    
    :param params:
    :return:
    """
    
    def quote_if_needed(in_: str) -> str:
        """
        Surrounds a string with quote marks if it contains whitespace.
        """
        
        if True in [c in in_ for c in string.whitespace]:
            in_ = '"{}"'.format(in_)
        return in_

    query_parts = []
    for p in params:
        # ignore parameters related to sorting
        if 'sort' in p or 'page' in p:
            continue
        
        # add simple string directives without keyword
        if 'string' in p:
            query_parts.append(quote_if_needed(p['string']))
            continue
            
        # add keyword parameters
        assert len(p) >= 1
        keyword, value = list(p.items())[0]
        query_parts.append('{}:{}'.format(
                keyword,
                quote_if_needed(value)
        ))
    
    return ' '.join(query_parts)


def search(request, url_remainder: str):
    """
    Search by a query defined in the url and displays the relevant infos.

    :param request: the request object from django
    :param url_remainder: str containing the part of the url with the search parameters
    :return: rendered results page
    """
    # TODO: add explanation of search params code in searches
    # this means explanations for rvk codes, details to authors, …

    # unescape the url and extract the parameters
    url_remainder = unquote(url_remainder)
    params = extract_params_from_url(url_remainder)

    results = {}
    pagination = []
    errors = []
    response = None

    try:
        response = load_book_information(params)
    except requests.RequestException as e:
        errors.append(_('Es ist ein Fehler beim Laden der Suchergebnisse aufgetreten: ') + str(e))

    if response is None:
        # didn't receive search results but display the pagination anyway
        pagination = generate_pagination(params, False)
    else:
        results = response['results']
        next_page = response['next_page']

        # generate the pagination
        pagination = generate_pagination(params, next_page)

    if 'author' in params[0]:
        # TODO: improve this test, only works if author is the first property
        author_information = get_author_information(params[0]['author'])
    else:
        author_information = False

    return render(
            request,
            'search/results.html',
            {
                'results': results,
                'query_string': generate_query_string_from_params(params),
                'author_information': author_information,
                'pagination': pagination,
                'errors': errors
            }
    )
