from django.test import TestCase
from search.views import extract_params_from_url, generate_query_string_from_params, generate_pagination


# Create your tests here.

class Test_Search(TestCase):
    def test_extract_params_from_url(self):
        self.assertEqual(extract_params_from_url(''), [])
        self.assertEqual(extract_params_from_url('rvk/GE 5517'), [{'rvk': 'GE 5517'}])
        self.assertEqual(extract_params_from_url('author/Segebrecht, Wulf'), [{'author': 'Segebrecht, Wulf'}])
        self.assertEqual(extract_params_from_url('author/Segebrecht, Wulf/sort/title-desc'),
                         [{'author': 'Segebrecht, Wulf'}, {'sort': 'title-desc'}])
        with self.assertRaises(AttributeError):
            extract_params_from_url(None)
        with self.assertRaises(AttributeError):
            extract_params_from_url(123)

    def test_generate_query_string_from_params(self):
        self.assertEqual(generate_query_string_from_params([{'author': 'Segebrecht, Wulf'}]),
                         'author:"Segebrecht, Wulf"')
        self.assertEqual(generate_query_string_from_params([{'rvk': 'GE 5517'}]), 'rvk:"GE 5517"')
        self.assertEqual(generate_query_string_from_params(''), '')
        self.assertEqual(generate_query_string_from_params([{'author': 'Segebrecht, Wulf'}, {'sort': 'title-desc'}]),
                         'author:"Segebrecht, Wulf"')
        with self.assertRaises(AttributeError):
            generate_query_string_from_params('123')
        with self.assertRaises(TypeError):
            generate_query_string_from_params(None)
        with self.assertRaises(TypeError):
            generate_query_string_from_params(123)

    def test_generate_pagination(self):
        self.assertEqual(generate_pagination([{'rvk': 'GE 5517'}], False), [])
        self.assertEqual(generate_pagination([{'rvk': 'BQ 4900'}, {'page': '2'}], False), [
            {
                "text": "erste Seite",
                "target": "/search/rvk/BQ 4900",
                "active": False
            },
            {
                "text": "1",
                "target": "/search/rvk/BQ 4900",
                "active": False
            },
            {
                "text": "2",
                "target": "/search/rvk/BQ 4900/page/2",
                "active": True
            }
        ])
        self.assertEqual(generate_pagination([], True), [
            {
                "text": "erste Seite",
                "target": "/search",
                "active": True
            },
            {
                "text": "1",
                "target": "/search",
                "active": True
            },
            {
                "text": "nächste Seite",
                "target": "/search/page/2",
                "active": False
            }
        ])
        with self.assertRaises(TypeError):
            generate_pagination(None, None)
        self.assertEqual(generate_pagination([], None), [])
        