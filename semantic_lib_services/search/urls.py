from django.urls import re_path, path
from django.views.generic import TemplateView

from .views import search, receive_search_query

urlpatterns = [
	path('', TemplateView.as_view(template_name='search/index.html')),
	path('receive', receive_search_query),
	re_path(r'(?P<url_remainder>.*)', search),
]


