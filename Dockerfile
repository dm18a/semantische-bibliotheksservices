# https://docs.docker.com/engine/reference/builder/ explains the sytax of this file

FROM python:3.5
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
VOLUME /code
WORKDIR /code
RUN pip install pipenv
COPY ./Pipfile /code/Pipfile
COPY ./Pipfile.lock /code/Pipfile.lock
RUN pipenv install --deploy --system --dev
