# Semantische Bibliotheksservices

## offizielle Beschreibung

### Betreuer
- [Dorian Merz](mailto:merz@ub.uni-leipzig.de) (UB Leipzig)
- [Martin Czygan](mailto:martin.czygan@uni-leipzig.de) (UB Leipzig)

### Tutor
- Malte Tammena

### Kurzbeschreibung
Die etliche Millionen Titel umfassenden Bestände der UB Leipzig sind bisher über den Katalog gut recherchierbar – aber nur unzureichend maschinell nachnutzbar. Daher werden umfangreiche Bibliotheksdaten momentan zu Linked Open Data konvertiert und in einem Triplestore zur Verfügung gestellt, der über einen offenen SPARQL-Endpunkt verfügt. Genauso werden die Stammdaten der UB Leipzig, das heißt im wesentlich Adressdaten und Öffnungszeiten der verschiedenen Standorte, als LOD zur Verfügung gestellt. Zusätzliche APIs machen Informationen zu Exemplaren und Verfügbarkeit der physischen Ressourcen über HTTP als LOD abrufbar. Diese weitreichende Publikation der Bibliotheksdaten in verbreiteten Datenschemata soll einerseits die Bibliothek transparenter machen, vor allem aber neue Wege der automatisierten Nachnutzung von Daten eröffnen.

### Ziel
Ziel dieser Praktikumsaufgabe ist die Entwicklung wahlweise einer Webanwendung oder mobilen App, welche die Daten der UB mit anderen als LOD verfügbaren, offenen Daten (z. B. dbpedia, wikidata, Geoinformationen) kombiniert und so vernetzte semantische Recherchen ermöglicht. Genaue Vorgaben für die Art dieser neuen Services gibt es nicht – hier ist die Kreativität des Teams gefragt. Denkbar ist alles – von einer praktischen App mit standortbezogenen Karten der am nächsten gelegenen geöffneten Zweigstellen, in denen ein bestimmtes Exemplar momentan ausleihbar ist, bis hin zu Spielereien wie einer Suchoberfläche für semantische Abfragen, die etwa auch die Geburtsjahre und -orte der Autoren in die Recherche einschließen.

### Lernziele
- Browseranwendungsentwicklung
- App-Entwicklung
- Semantische Technologien (RDF, SPARQL, Linked Data)

## Installation und weitere Hinweise

siehe HELP.md
