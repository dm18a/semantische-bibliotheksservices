# Releaseplan

## Releasebündel 1 – Proof of concept

*Abgabe: 07.01.2019*

Das erste Releasebündel dient als proof of concept und wird damit eine erste benutzbare, minimale Version des Projektes darstellen. Es werden nur die wichtigsten Seite mit reduzierter Funktionalität implementiert um dem Raum für mögliche Fehler zu reduzieren und erste Praktische Tests des Konzeptes mit Usern zu ermöglichen.

- **/LF0110/**: Eingabe
- **/LF0210/**: Ergebnisliste
- **/LF0230/**: Detailansicht
- **/LL0010/**: Mobile Ansicht (prototypisch)
- **/LL0020/**: Erreichbarkeit der Seiten

noch offene Punkte
- **/LF0330/**: Lokalisierung der Bibliothek
- **/LF0120/**: Suche
- **/LF0240/**: Erneute Suche


## Releasebündel 2 – Muss-Ziele erfüllt

*Abgabe: 21.01.2019*

Im zweiten Releasebündel werden die meisten übrigen obligatorischen Ziele erfüllt. Dadurch wird die Seite in den minimalen benutzbaren Zustand versetzt. Die Dokumentation und die meisten Qualitätssicherungskonzepte sind implementiert und dienen als Basis für den weiteren Ausbau der Software.

aus Releasebündel 1:
- **/LF0330/**: Lokalisierung der Bibliothek
- **/LF0120/**: Suche
- **/LF0240/**: Erneute Suche


- **/LF0340/**: Lokalisierung innerhalb der Zweigstelle Albertina
- **/LL0010/**: Mobile Ansicht
- **/LL0030/**: Stabilität
- **/LL0050/**: Barrierefreiheit
- **/LL0060/**: Multilingualität
- **/LL0070/**: Hilfetexte (für bisher Implementiertes)


noch offen:
- **/LF0220/**: Verfügbarkeitsanzeige in Suchergebnissen
- **/LF0320/**: Verfügbarkeit eines Werkes in Zweigstellen
- **/LF0310/**: Informationen zum Werk (ohne Bilder)


## Releasebündel 3 – erste optionale Ziele

*Abgabe: 04.02.2019*

Releasebündel 3 enthält die nötigen Features um die Software auf das ursprünglich gewünschte Niveau zu bringen. Es werden dabei sehr hilfreiche aber grundsätzlich optionale Features wie Filter für die Suche oder umfangreichere Informationen zu Werken sowie die noch fehlenden obligatorischen Features implementiert.


aus Releasebündel 2:
- **/LF0220/**: Verfügbarkeitsanzeige in Suchergebnissen
- **/LF0320/**: Verfügbarkeit eines Werkes in Zweigstellen
- **/LF0310/**: Informationen zum Werk (ohne Bilder)


- **/LF0130/**: Erweiterte Suche
- **/LF0150/**: Filter
- **/LF0250/**: Entfernung (Distanz zur Bibliothek)
- **/LF0260/**: Sortierung (der Ergebnisliste)
- **/LF0270/**: Verlinkung auf relevante Suchen
- **/LF0310/**: Informationen zum Werk (mit Bildern, wenn nutzbare Bilder vorhanden sind)
- **/LF0340/**: Lokalisierung innerhalb der Bibliothek

noch offen:
- **/LF0220/**: Verfügbarkeitsanzeige in Suchergebnissen
- **/LF0320/**: Verfügbarkeit eines Werkes in Zweigstellen
- **/LF0150/**: Filter
- **/LF0260/**: Sortierung (der Ergebnisliste)
- **/LF0310/**: Informationen zum Werk (Bilder zum Werk ausgeben)


## Releasebündel 4 – restliche Kann-Ziele

*Abgabe: 04.03.2019*

Das vierte Releasebündel wird das letzte reguläre Releasebündel sein in dem Features implementiert werden. Die dabei eingeplanten Funktionen drehen sich nur noch um den Komfort bei der Nutzung der Anwendung und sollen den User weiter beim Finden und Leihen eines Werkes aus dem Bestand der Universitätsbibliothek Leipzig unterstützen.

aus Releasebündel 3:
- **/LF0150/**: Filter
- **/LF0260/**: Sortierung (der Ergebnisliste)


- **/LF0280/**: Autorenauswahl (in der Suche)


Verschoben in Releasebündel 5:
- **/LF0220/**: Verfügbarkeitsanzeige in Suchergebnissen
- **/LF0320/**: Verfügbarkeit eines Werkes in Zweigstellen (verschoben aufgrund fehlenden Kartenmaterials)
- **/LF0140/**: Fehlererkennung bei der Suche
- **/LF0341/**: Regalgenaue Lokalisierung innerhalb der Bibliothek
- **/LF0350/**: Autorenauswahl (in der Detailansicht)
- **/LF0410/**: Anzeige von Stammdaten (von Autoren)
- **/LF0420/**: Bibliographie mit Verfügbarkeiten
- **/LF0310/**: Informationen zum Werk (Bilder zum Werk ausgeben)

## Releasebündel 5 – Bugfixes und Optimierungen

*Abgabe: 25.03.2019*

Im fünften und letzten regulären Releasebündel sind derzeit nur grundsätzliche Optimierungen in Bezug auf Geschwindigkeit und Zuverlässigkeit des Dienstes sowie Bugfixes eingeplant. Dadurch wird gegebenenfalls Raum zur Implementierung von Features gegeben die ansonsten aufgrund unerwarteter Schwierigkeiten nicht im finalen Produkt enthalten sein könnten.

aus früheren Releasebündeln:
- **/LF0350/**: Autorenauswahl (in der Detailansicht)
- **/LF0420/**: Bibliographie mit Verfügbarkeiten

- **/LL0040/**: Geschwindigkeit
- **/LL0070/**: Hilfetexte vervollständigen


wird in R5.1/Endabgabe implementiert:
- **/LF0410/**: Anzeige von Stammdaten (von Autoren)
- **/LF0310/**: Informationen zum Werk (Bilder zum Werk ausgeben)


wird nicht implementiert:
- **/LF0320/**: Verfügbarkeit eines Werkes in Zweigstellen (verschoben aufgrund fehlenden Kartenmaterials)
- **/LF0140/**: Fehlererkennung bei der Suche
- **/LF0341/**: Regalgenaue Lokalisierung innerhalb der Bibliothek

## Endabgabe / R5.1

*Abgabe: 04.04.2019* 

In der Endabgabe ist zusätzlich zu den unerwarteterweise nicht fertig gewordenen Lastenpunkten die Präsentation für die Endabgabe enthalten. Zusätzlich wurde das Repository ein letztes mal aufgeräumt und die Dokumente soweit relevant aktualisiert.

in der Endabgabe implementiert:
- **/LF0410/**: Anzeige von Stammdaten (von Autoren)
- **/LF0310/**: Informationen zum Werk (Bilder zum Werk ausgeben)
