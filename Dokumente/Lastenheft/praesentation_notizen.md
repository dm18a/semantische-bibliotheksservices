# Suchfenster

## Prototyp
- Suchzeile für die Eingabe der Buchsignatur.
- Auswahl via Dropdown-Menü 
- Klick auf den Button startet die Suche

## Mindestziel

## Mögliche Verbesserungen
- Suchzeile für die Eingabe der Buchsignatur, des Autors, des Titels oder der ISBN.
- Fehlerkorrektur bei der Eingabe (Tippfehler, ungenaue Angaben)

# Ergebnisliste

## Prototyp
- Anzeige der Suchergebnisse mit Titel, Autor und Verfügbarkeit (muss gesondert abgefragt werden).
- Durch Klick auf den Treffer wird die Detailansicht angezeigt
- Erneute Suche ist auf derselben Seite möglich

## Mindestziel

## Mögliche Verbesserungen
- Anzeige des geographisch nächsten Buches zum Nutzer (via GPS-Daten)
- Sortierung der Ergebnisse
- Anzeige ähnlicher Suchen
- Auswahl eines Autoren anstatt eines Buches

# Detailansicht

## Prototyp
- Anzeige der Zweigstellen, in denen sich das Buch befindet auf einer Karte.
- Verfügbarkeit des Buches
- Titel des Buches

## Mindestziel
- Weitere Informationen zum Werk (Autor, Erscheinungsjahr, Link zum Katalogeintrag, Bild (wenn vorhanden))
- Bereichsgenaue Lokalisierung innerhalb der Bibliothek, bei Klick auf Exemplar oder Zweigstelle

## Mögliche Verbesserungen
- Regalgenaue Lokalisierung des Buches
- Auswahl des Autors

# OPTIONAL: Autorenseite
## Möglicher Inhalt
- Anzeige von Stammdaten (z.B. Geburtsdatum, Wirkungsorte...)
- Anzeige aller Werke in der Bibliothek, mit denselben Möglichkeiten wie bei Einzelwerken.

# Zusätzliche Seiten
- Impressum
- Kontakt
- About
- Link zum Blog
