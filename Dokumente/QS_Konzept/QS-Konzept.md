# Qualitätssicherung

## 1 Dokumentationskonzept

Die gründliche Dokumentation des Projekts ist für dessen Erfolg bedeutend. Daher werden im Folgenden Dokumentationskonventionen festgelegt, um eine im Team standardisierte Dokumentation gewährleisten zu können.

### 1.1 Quellcode
Generell gilt für die Dokumentation, dass sie dem Code nicht widerspricht. Das bedeutet auch, dass die Dokumentation bei einer Veränderung des Codes ebenfalls angepasst werden muss. Zudem soll grundsätzlich auf Englisch dokumentiert werden, um möglichst vielen die Möglichkeit zu bieten, das Programm zu lesen und zu verstehen.

#### 1.1.1 Quelltextnahe strukturierte Dokumentation
Der Zugang zum Projekt soll sowohl für Teammitglieder, die bisher an anderen Aspekten gearbeitet haben, als auch für externe Programmierer so leicht wie möglich sein. Dafür wird eine quelltextnahe strukturierte Dokumentation erzeugt, welche jegliche Klassen und Funktionen beschreibt. Damit die Dokumentation übersichtlich und leicht navigierbar ist, wird sie mit `pydoc` erzeugt. 

#### 1.1.2 Interne Dokumentation
Inline-Kommentare im Quellcode sind nur zu verwenden, um funktionsinterne Abschnitte zu erklären, die nicht direkt offensichtlich sind, wie z.B. die Implementierung von Algorithmen. Die Kommentare sollen kurz gefasst werden und nur die wichtigsten Informationen enthalten. Grundsätzlich sollen Kommentare über der zu kommentierenden Zeile verfasst werden.

### 1.2 Entwurfsbeschreibung
Die Entwurfsbeschreibung dient qualifizierten Dritten, die sich in das Projekt einarbeiten möchten, und soll wichtige Modellierungs-, Strukturierungs- und Designentscheidungen erläutern. Sie dient als erster Kontaktpunkt zum Projekt. Die Beschreibung wird im Laufe der Entwicklung des Projekts geschrieben und aktualisiert.

### 1.3 Coding Standard
Mit Coding Standards wird eine generelle Stilkonvention für den Quelltext festgelegt, an die sich alle Entwickler im Projekt halten. Dies erhöht die Lesbarkeit und erleichtert das Verstehen des Codes. Für die Pythonentwicklung wird auf den PEP 8 Style Guide und für die Frontendentwicklung in JavaScript auf [standard.js](https://standardjs.com/) zurückgegriffen. Beides wird durch die GitLab CI überprüft.

## 2 Testkonzept
Um eine möglichst fehlerfreie Software zu entwickeln, ist es erforderlich ihr Verhalten zu testen. Da das Testen sehr viel Zeit in Anspruch nehmen kann, wenn es manuell gemacht wird, greifen wir auf automatisierte Tests zurück. Dabei werden Tests für möglichst jede neu hinzugefügte Funktion geschrieben. Im Backendbereich nutzen wir für Tests die integrierte Testsuite von Django. Im Frontend werden wir [Jest](https://jestjs.io/) für JavaScript verwenden. Beide sollen in der GitLab-CI-Pipeline integriert sein und damit jeden Commit automatisch testen, bevor dieser auf das Projekt angewandt wird.

Über die automatisierten Tests hinaus werden wir auch das Verhalten der Applikation, besonders im Hinblick auf die Benutzbarkeit, manuell testen. Dabei können unter Umständen auch Grenzfälle entdeckt werden, die unsere Tests noch nicht abdecken, aber fehlerhaftes Verhalten auslösen. Diese werden dann selbstverständlich über den Issuetracker gemeldet, behoben und zur Testsuite hinzugefügt, um ein erneutes Auftreten zu verhindern.

## 3 Organisatorische Festlegungen

### 3.1 Treffen
Das Treffen mit den Betreuern und dem Tutor findet jede Woche am Donnerstag um 11:15 Uhr in der Albertina statt. Hierbei werden offen gebliebene Fragen geklärt und die Aufgaben für die nächste Abgabe besprochen. Außerdem wird über die vergangene Abgabe geredet und Verbesserungen angemerkt. Zudem findet ein Teamtreffen mindestens einmal in der Woche statt. Damit das weitere Vorgehen, Probleme und Fragen einfach geklärt werden können, werden am Anfang jedes Treffens Tagesordnungspunkte festgelegt.

### 3.2 Protokolle
Bei jedem Treffen wird von einem Teammitglied ein Protokoll geführt. Dieses wird dann dem ganzen Team im Git-Repository bereitgestellt. Das Protokoll soll folgende Punkte beinhalten:

- Datum
- Beginn und Ende des Treffens
- Anwesenheit
- offene und geklärte Fragen, Entscheidungen
- Inhaltliche Zusammenfassung

### 3.3 Interne Kommunikation
Zur Kommunikation untereinander nutzen wir Slack, um Termine oder inhaltliche Themen zu diskutieren. Durch die verschiedenen Channels kann sich sehr unkompliziert auch in kleineren Gruppen ausgetauscht werden.

### 3.4 GitLab
Alle Dokumente und der Quellcode werden in unserem Git-Repository gespeichert. Neben dem Master-Branch, der das aktuelle Release enthält, gibt es einen Develop-Branch für die aktuelle Entwicklung und ggf. einzelne Feature-Branches. Commits sollten regelmäßig erfolgen, sodass der Entwicklungsfortschritt nachvollziehbar bleibt. Aufgaben werden mithilfe der Issues von GitLab verteilt und deren Zustand mit Labeln gekennzeichnet. Weiterhin können im Wiki des GitLab wichtige Informationen hinterlegt werden.
