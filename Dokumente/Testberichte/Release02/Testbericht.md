# Testbericht

*Till Mahlburg*

## Vorrichtungen zum Testen

### Automatisierte Tests

Um sicherzustellen, dass funktionierender Code auch weiterhin funktioniert, verwenden wir zwei Test-Suites. Dabei handelt es sich um die integrierte Test-Suite, die unser Webframework Django bereits mitliefert um Python-Code zu testen und um JEST, das in der Lage ist JavaScript zu testen. Darüber hinaus nutzen wir Flake8, um Syntax-, Stil- und Formatierungsfehler im Pythonquelltext zu entdecken und zu beheben. Flake8 hilft uns momentan auch am meisten, weil es Konflikte zum Code-Stil verhindert und einen einheitlichen Stil erzwingt.

Djangos Testsuite haben wir bereits ein paar Tests hinzugefügt, die aber erst einige wenige Funktionen abdecken. Bei vielen Funktionen haben wir das Problem, dass sie von der Datenbasis auf dem Bibliotheksserver abhängen. Dadurch können sie nicht ohne weiters von GitLab-Runnern automatisiert getestet werden. Das ist besonders auch deshalb der Fall, weil der Server von einer Firewall geschützt wird und dadurch vor dem Zugriff der Runner geschützt. Für dieses Problem haben wir bisher noch keine Lösung, aber wir wollen die Situation gern verbessern.

Da sich unser JavaScript-Code momentan auf die reine Konfiguration des Leaflet-Plugins zur Kartenanzeige beschränkt, haben wir hier bisher auf Tests verzichtet. Sollte aber in Zukunft mehr JavaScript verwendet werden, ist die Test-Suite fertig eingerichtet und bereit.

### Manuelle Tests

Manuelle Tests werden momentan noch nicht geregelt ausgeführt. Das heißt aber nicht, dass gar keine gemacht werden. Jedes Teammitglied testet nach Möglichkeit die selbst implementierten Funktionen auf Korrektheit. Wenn der Prototyp etwas weniger Änderungen in Oberfläche und Bedingung unterworfen ist, sollte aber ein besser strukturierter Testvorgang entworfen und regelmäßig ausgeführt werden.

## Testergebnisse

### Automatisierte Tests

Das Ergebnis unser Testsuite kann im [GitLab][1] für alle Branches angesehen werden.

### Manuelle Tests

Geregelte manuelle Tests wurden mit dem aktuellen Prototypen noch nicht durchgeführt, daher gibt es auch noch keine Ergebnisse. Daran wollen wir in Zukunft arbeiten.

## Fortschritt im Releaseplan

Nach der Abnahme durch Betreuer wird unser Fortschritt im Releaseplan angepasst.


[1]:https://git.informatik.uni-leipzig.de/swp18/dm18a/pipelines?scope=branches
