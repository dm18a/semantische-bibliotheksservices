21.10.18 | dm18a | Tom Winter

# Stärken und Schwächen der Teammitglieder

## Tom Winter

### Stärken
- Software Engineering (Python, Java, Javascript)
- Backendentwicklung

### Schwächen
- UX

## Victor Jüttner

### Stärken
- gute Kenntnisse: Java
- Grundkenntnisse: Python/Lisp/Prolog/C
- Präsentieren

### Schwächen
- Theoretische Informatik

## Till Mahlburg

### Stärken
- Linux
- Java geht gut, Python läuft auch
- Shellscripting

### Schwächen
- Mathe
- Datenbanken

## Katharina Waxweiler

### Stärken 
- gute Java-Kenntnisse
- Basic Kenntnisse in Programmiersprachen aus MUP2
- Erfahrung in Frontendentwicklung

### Schwächen
- Präsentieren

## Magnus Winkler

### Stärken
- gute Java und C# Kenntnisse
- Basis C++ Kenntnisse

### Schwächen
- Präsentieren
- wenig Erfahrung im Programmieren als Teil eines Teams

## Julia Sobol

### Stärken
- Java, sowie Grundkenntnisse in Sprachen aus MUP 2
- ein bisschen Erfahrung in Frontend
- Grundkenntnisse in LaTex

### Schwächen
- Präsentieren
- Rechtschreibung
- Wenig Erfahrung in Programmieren
 
## Felix Cäsar Madorskiy

### Stärken
- mittlere Pythonkenntnisse
- Grundkenntnisse in Java, C
- mittlere Kenntnisse in Latex
- rudimentäre Kenntnisse in Fortran, MuP II Programmiersprachen
- Mathematik
- Eklären, Vermitteln, Präsentieren

### Schwächen
- Systemadminestration, Linux
