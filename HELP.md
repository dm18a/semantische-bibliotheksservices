# Hilfe

## Benutzung mit Docker

### Installation

Die folgende Anleitung ist unter Linux (genauer: Arch) getestet, sollte aber grundsätzlich auch unter Windows funktionieren.

1. installiere docker und docker-compose
2. richte docker ein ([einfache Anleitungen für alle unterstützten Plattformen](https://docs.docker.com/install/#supported-platforms), [Anleitung für Arch](https://wiki.archlinux.org/index.php/Docker), [Docker automatisch bei Systemstart ausführen](https://docs.docker.com/install/linux/linux-postinstall/#configure-docker-to-start-on-boot)), es sind root-Rechte für dessen Nutzung nötig

### Start

1. starte die Container für das Projekt mit `docker-compose up` im Stammverzeichnis des Projektes
2. die Seite ist auffindbar unter http://0.0.0.0:8000

## Benutzung ohne Docker

### Installation

1. installiere python in Version 3.5 und pipenv für python 3
2. installiere die Bibliotheken für das Projekt mit `pipenv install` im Stammverzeichnis des Projektes

### Start

1. aktiviere die virtuelle Umgebung mit `pipenv shell` im Stammverzeichnis des Projektes
2. wechsle in das Programmverzeichnis mit `cd semantic_lib_services`
3. starte den django Testserver mit `python manage.py runserver 0:8000`
4. die Seite ist erreichbar unter http://0.0.0.0:8000

## Testen

Zum Testen des Projektes sind unittests im Projekt integriert. Damit diese auch für externe APIs zuverlässig funktionieren sind mock-up dieser externen Dienste ebenfalls integriert. Um diese zu nutzen ist die Nutzung einer speziellen Projektkonfiguration nötig.
Der Start des Django-Entwicklungsservers mit der Projektkonfiguration zum Testen der APIs geschieht mit `python manage.py runserver 0:8099 --settings semantic_lib_services.settings_test` im Verzeichnis mit den Programmdaten (Ordner mit manage.py).

## Hinweise zu den einzelnen Tools

### docker

- `docker help` für die Hilfe
- `docker ps -a` um alle Container anzuzeigen
- `docker stop <name oder id>` un container zu stoppen
- `docker remove <name oder id>` um container zu entfernen
- `docker system prune -a` um alle ungenutzten Images, Volumes, Netzwerke, … im Cache von Docker zu entfernen

### docker-compose

Werkzeug um mehrere Container von Docker einfacher zu starten und zu verwalten.

- `docker-compose help` zeigt die Hilfe zu docker-compose an
- `docker-compose up` um die eingestellten services zu starten
- `docker-compose up -d` um Obiges um den detached mode zu nutzen (die Ausführung der Container ist damit nicht mehr an das Terminal-fenster gebunden)
- `docker-compose down` um die Ausführung im detached mode zu beenden
- `docker-compose run <container id> <command>` um einzelnen Befehl im Container auszuführen
- `docker-compose exec <id of container> bash` für eine interaktive Terminal-Sitzung in benannten Container
- `docker-compose build` um die Container neu aufzubauen (nur bei Änderungen an der Projektumgebung nötig, z.B. beim Einfügen neuer Bibliotheken)

### django

Alle im Folgenden genannten Befehle werden in der virtuellen Umgebung im Ordner semantic_lib_services ausgeführt.

- wurde die Models verändert müssen die migrations von django erstellt und angewendet werden. Dies geschieht mit `python manage.py makemigrations` und `python manage.py migrate`. Details dazu unter https://docs.djangoproject.com/en/2.1/topics/migrations/

#### Übersetzung
Django hat die Funktion markierten Text zu übersetzten. In der Datei django.po kann man dem markierten Text eine Übersetzung geben.
- `django-admin makemessages -l en` erstellt oder aktualisiert die Datei
- `django-admin compilemessages` um die hinzugefügte Übersetzung zu speichern

Mehr Details dazu unter https://docs.djangoproject.com/en/2.1/topics/i18n/translation/

### pipenv

- eine shell in der virtuellen Umgebung bekommt man mit `pipenv shell`
- einzelne Befehle führt man mit `pipenv run <Befehl>` aus
- wurde Pipfile geändert werden die Aktualisierungen mit `pipenv update` übernommen
