---
layout: post
title:  "Lastenheft"
---

# Lastenheft

## 1. Visionen und Ziele

Semantische Netzwerke ermöglichen vernetzte Zusammenarbeit und intelligente Entscheidungsfindungen, die jenseits der Möglichkeiten des aktuellen Internets liegen. Durch die Verknüpfung von Webseiten und Datenbanken über eine Reihe semantischer Anwendungen können Computer effizient auf Anfragen von Benutzern reagieren – und so inhaltlich bessere Ergebnisse liefern. Suchmaschinen können so ihre Anzeigen ansprechender nach Relevanz sortieren, gegebenenfalls direkt auf gesuchte Frage antworten oder zusätzliche Informationen bieten. Wir erweitern die Suche nach Büchern aus der Universitätsbibliothek Leipzig (UB) um eine Geolokalisierung. Dem Nutzer wird auf einer Karte angezeigt, in welchem Bibliotheksstandort sich das gesuchte Werk befindet und ob es dort verfügbar ist. Weiter möchten wir für ausgewählte Standorte die Lokalisierung eines Buches innerhalb der Bibliothek ermöglichen, um so die Suche zu vereinfachen.

## 2. Rahmenbedingungen und Produkteinsatz

Die aktuelle Suche der UB liefert noch keine weiterführenden Informationen zu den Inhalten. Unserer Suchmaschine, die eine Ortung anbietet, richtet sich an diejenigen, die die jetzige Suche schon verwenden und insbesondere an User, die nicht ortskundig sind oder nicht alle Bibliotheksstandorte kennen. Mit der benutzerfreundlichen Webanwendung kann die Suche von jedem beliebigen Endgerät gestartet werden.

## 3. Kontext und Überblick


## 4. Funktionale Anforderungen

Die funktionalen Anforderungen werden in die einzelnen Seitenansichten eingeteilt. Zuerst werden die Muss-Ziele vorgestellt und anschließend optionale Kann-Ziele erläutert.

### 4.1 Erforderlich

Diese Ziele sind erforderlich für die vollständige Nutzbarkeit des Produktes und müssen demzufolge komplett implementiert werden.

#### 4.1.1 Suche

###### /LF0110/ Eingabe
Ein Nutzer kann einen Suchbegriff in die Suchzeile eingeben.

###### /LF0120/ Suche
Nachdem der Nutzer einen Suchbegriff eingegeben hat, kann er auf einen Button neben der Suchzeile drücken um die Suche zu starten. Daraufhin wird die Ergebnisliste angezeigt. Bei leerer Suchzeile hat es keine Wirkung, den Button zu klicken.

#### 4.1.2 Ergebnisliste

###### /LF0210/ Liste
Es werden Titeldatensätze angezeigt, die über den Suchbegriff gefunden wurden. Gefunden werden Treffer mit Übereinstimmung von Suchbegriff und Titel bzw. Suchbegriff und Autor. Angezeigt werden der Titel und der Autor.

###### /LF0220/ Verfügbarkeitsanzeige
Bei jedem Treffer wird angezeigt, ob der Titel zur Ausleihe verfügbar ist.

###### /LF0230/ Detailansicht
Ein Nutzer kann durch klicken auf einen Treffer zu der dazugehörigen Detailansicht wechseln.

###### /LF0240/ Erneute Suche
Ist der Nutzer unzufrieden mit seinen Suchergebnissen, hat er die Möglichkeit erneut zu suchen. Die Suchzeile mit der Eingabe zur aktuellen Suche und der Button zum Suchen sind im Kopf der Seite zu finden.

#### 4.1.3 Detailseite

###### /LF0310/ Informationen zum Werk
Auf der Detailseite werden zusätzlich zum Titel und dem Autor eines Titeldatensatzes auch das Erscheinungsjahr, der Link zum Katalogeintrag und, wenn vorhanden, ein Bild des Werkes angezeigt.

###### /LF0320/ Verfügbare Exemplare
Es werden alle Ausgaben des Titels, welche in allen Zweigstellen der UB zu finden sind, angezeigt. Zu jeder Zweigstelle wird angezeigt ob es derzeit ausleihbar ist.

###### /LF0330/ Lokalisierung der Bibliothek
Es wird zusätzlich eine Karte angezeigt, welche die Standorte der Zweigstellen der UB, die den Titel führen, in Leipzig darstellt.

###### /LF0340/ Lokalisierung innerhalb der Bibliothek
Ein Nutzer kann auf eines der verfügbaren Exemplare oder eine Zweigstelle klicken, woraufhin ihm ein Raumplan der entsprechenden Bibliothek angezeigt wird. Der Bereich in dem sich das Exemplar befindet wird dabei markiert.

### 4.2 Optional

Folgende Features sind wünschenswert, müssen aber nicht zwingend implementiert werden. 

#### 4.2.1 Suche

###### /LF0130/ Erweiterte Suche
Ein Nutzer kann angeben, wonach gesucht wird. Möglichkeiten sind Titel, Autor, die Signatur oder die ISBN. Dies würde /LF0210/ verändern, da nur noch Treffer in der angegebenen Kategorie angezeigt werden.

###### /LF0140/ Fehlererkennung
Die Suche soll eine Korrektur oder Verbesserung falsch geschriebener oder ungenauer Suchanfragen vorschlagen.

###### /LF0150/ Filter
Ein Nutzer bekommt die Möglichkeit, seine Suche nach verschiedenen Ergebnissen zu filtern. Denkbar wären z.B. Filter zur Verfügbarkeit, Zweigstellen, nach Autor oder Themenbereichen.

#### 4.2.2 Ergebnisliste

###### /LF0250/ Entfernung
Wenn eine Standortinformation des Nutzers vorhanden ist, wird die Entfernung zur nächsten Zweigstelle, in der der Titel verfügbar ist, angezeigt.

###### /LF0260/ Sortierung
Die Ergebnisliste soll nach verschiedene Kriterien sortierbar sein. Denkbar sind z.B. Verfügbarkeit, Entfernung oder Erscheinungsjahr.

###### /LF0270/ Verlinkung auf relevante Suchen
Es werden Links zu ähnlichen Suchen angezeigt, beispielsweise zur gleichen Suche mit zusätzlichem Filter nach Verfügbarkeit oder Autor.

###### /LF0280/ Autorenauswahl
Ein Nutzer kann bei einem Suchergebnis den Autor auswählen und wird auf eine Autorenseite weitergeleitet.

#### 4.2.3 Detailseite

###### /LF0341/ Regalgenaue Lokalisierung innerhalb der Bibliothek
Der Punkt /LF0340/ wird für ausgewählte Zweigstellen der Bibliothek erweitert, sodass das Regal, in dem sich das gewählte Werk befindet, hervorgehoben wird.

###### /LF0350/ Autorenauswahl
Mit einem Klick auf den Autoren wird man auf eine Detailseite zum Autor weitergeleitet.

#### 4.2.4 Autorenseite

###### /LF0410/ Anzeige von Stammdaten
Es werden Stammdaten über den Autor angegeben, beispielweise Geburtsdatum oder Wirkungsorte.

###### /LF0420/ Bibliographie mit Verfügbarkeiten
Zudem werden die Werke des Autoren, welche in dem Katalog der UB sind, angezeigt. Diese Anzeige bietet die gleichen Möglichkeiten wie die Ergebnisanzeige in 4.1.2.

## 5. Produktdaten und nichtfunktionale Anforderungen

Auch hier folgt eine Einteilung in erforderliche Muss-Ziele und die optionalen Kann-Ziele.

### Erforderlich

###### /LL0010/ Mobile Ansicht
Die Seite muss einwandfrei auf mobilen Endgeräten funktionieren.

###### /LL0020/ Erreichbarkeit der Seiten
Jede der drei Seiten soll über einen konstanten Link erreichbar sein, so dass die Suche leicht geteilt werden kann und eine eventuelle Anbindung der Karte an den Katalog der UB leicht funktioniert.

###### /LL0030/ Stabilität
Der Service soll problemlos funktionieren und nicht durch Nutzereingaben abstürzen können.

###### /LL0040/ Geschwindigkeit
Die Suche soll nicht so langsam sein, dass es den Arbeitsfluss des Nutzers hindert.

### Optional

###### /LL0050/ Barrierefreiheit
Der Service soll von Nutzern unabhängig von kognitiven, physischen oder technischen Einschränkungen genutzt werden können.

###### /LL0060/ Multilingualität
Der Service soll in mehreren Sprachen zur Verfügung stehen.

###### /LL0070/ Hilfetexte
Dem Nutzer werden Texte zur Beschreibung der verfügbaren Funktionen und dem Umgang angeboten.

## 6. Qualitätsmatrix nach ISO 25010

| Qualitätsparameter  | Ausprägung      |
|---------------------|-----------------|
|Funktionalität       | Hoch            |
|Zuverlässigkeit      | Mittel          |
|Benutzbarkeit        | Hoch            |
|Effizienz            | Mittel          |
|Wartbarkeit          | Mittel          |
|Portabilität         | Mittel          |
|Kompatibilität       | Mittel          |
|Sicherheit           | Niedrig         |

## 7. Lieferumfang und Abnahmekriterien

### 7.1. Lieferumfang

Im Vordergrund des Lieferumfangs steht eine Webseite, welche auf mobilen Geräten und Computern benutzt werden kann.
Das Backend wird mindestens den Datenbestand der UB nutzen um nach Büchern zu suchen.
Eine vollständige Dokumentation wird ebenfalls zur Verfügung gestellt.

### 7.2. Abnahmekriterien

Das Hauptabnahmekriterium ist eine funktionsfähige Webseite mit Suchmaschine und deren Ergebnissen, sowie den dazu geplanten Features. Dazu gehört ebenfalls, dass die genannten Muss-Ziele im Bereich der funktionalen und nicht-funktionalen Anforderungen erfüllt sind. Die Ausführung der Kann-Ziele wird nach ihrem Wert und verfügbaren Kapazitäten festgelegt. Zusätzlich soll die Funktionalität der einzelnen Module durch Tests verifiziert werden.

## 8. Vorprojekt

Im Rahmen des Vorprojektes werden wir unsere App in ihrer Grundversion umsetzten, welche bis zum ersten Release fertig gestellt werden soll.

Für einen ersten Prototypen wird eine Website mit einer trivialen Suchmaschine erstellt, welche Werke aus dem Datensatz anhand ihrer RVK-Signatur erkennt. Weiterhin soll die Verfügbarkeit eines gefundenen Objekts bei der UBL ermittelt und mit dem Werk angezeigt werden. Dabei wird auf einer Landkarte der Standort der UB, in dem sich das Werk befindet, angezeigt.

Hosting der Website sowie Packaging sollen nur eine Nebenrolle einnehmen.

## 9. Glossar

**Feature**:
Eine Funktion einer Software.

**ISBN**:
Steht für Internationale Standard Buchnummer. Bezeichnet ein Buch weltweit eindeutig.

**Signatur in der UB**:
Identifiziert einen Titel eindeutig im Bestand der UB.

**Backend**:
Der Teil einer Software, die nicht für den Benutzer sichtbar ist.
