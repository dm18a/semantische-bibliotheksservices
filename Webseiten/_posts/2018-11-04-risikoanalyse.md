---
layout: post
title:  "Risikoanalyse"
---

Diese Webseite dient der Dokumentation unseres Softwaretechnikpraktikums. Die laufenden Aufgaben werden wir als Blogposts veröffentlichen, während unser Team und unsere Aufgaben unter [about](http://pcai042.informatik.uni-leipzig.de/~dm18a/jekyll/about) zu finden sind.

Der Zweck dieses ersten Posts ist die Veröffentlichung unserer [Risikoanalyse (PDF)](http://pcai042.informatik.uni-leipzig.de/~dm18a/Dokumente_Risikoanalyse.pdf).
