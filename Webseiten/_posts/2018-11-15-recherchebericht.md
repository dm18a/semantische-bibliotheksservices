---
layout: post
title:  "Recherchebericht"
---

# Recherchebericht

## Begriffe

### semantische Verknüpfung

Eine semantische Verknüpfung verbindet Begriffe anhand ihrer Bedeutung und Beziehung (Relationen) zueinander. Das Ziel dabei ist Informationen einfacher für Computer austauschbar und verwertbar werden indem z.B. Metainformationen hinzugefügt werden (steht "Müller" für den Namen oder den Beruf?) und Verbindungen hergestellt werden (das Objekt "Leipzig" ist mit dem Objekt "Sachsen" durch die Beziehung "Stadt in Bundesland" verknüpft).


### markdown

Markdown ist eine einfache Sprache um die Formatierung von Text zu definieren. Es wurde entworfen um unverarbeitet noch immer gut lesbar zu sein aber auch um einfach in andere Formate wie z.B. HTML oder PDF konvertierbar zu sein.

Die wichtigsten Elemente sind dabei die Folgenden:
```
# Überschrift
## kleinere Überschrift
### noch kleinere Überschrift
...

- unsortierte Liste
- weiteres Listenelement

1. sortierte Liste
2. weiteres Element

[Titel eines Links](https://url-des-links.de)

*fetter Text*
**kursiver Text**

`unformatierter Text`

> Zitat
```


### git

Git ist eine dezentrale, nicht-lineare Software zur Versionsverwaltung. Die Gesamtheit der Versionen (commits) und damit aller erfassten Dateien bilden ein Repository. Synchronisierung des lokalen Repositories mit anderen Instanzen davon erfolgt entweder durch Download (pull) oder Upload (push) der jeweiligen Änderungen.

Eine der hilfreichsten Grundregeln im Umgang mit git lautet ["Commit early, commit often"](http://www.databasically.com/2011/03/14/git-commit-early-commit-often/).


### SPARQL

SPARQL steht für **S**PARQL **P**rotocol **A**nd **R**DF **Q**uery **L**anguage. Es handelt sich dabei also um eine Anfragesprache für RDF-Daten. 2008 wurde SPARQL als Recommendation vom World Wide Web Consortium freigegeben.


### Slack

Slack ist eine Anwendung die zur Kommunikation in Teams dient. Slack bietet die Möglichkeit, Kommunikation in Gruppenkanälen oder direkt zwischen einzelnen Teammitgliedern durchzuführen. Slack findet in der Industrie durchaus Verwendung. Firmen wie Oracle, Ebay oder Autodesk nutzen Slack zur Kommunikation. Slack bietet eine kostenfreie Version, in die sich bis zu zehn Erweiterungen wie z.B. eine ToDo-Liste integrieren lassen.

### Client-Server-Architektur

Konzept der Verteilung von Aufgaben und Dienstleistungen innerhalb eines Netzwerkes, wobei Aufgaben von Programmen erledigt werden, welche in Clients und Server aufgeteilt werden.

### Representational State Transfer (REST)

REST bezeichnet ein Programmierparadigma für verteilte Systeme, welches eine Abstraktion von Struktur und Verhaltens des Internet darstellt. Es stellt durch seine Prinzipien eine einheitlichen Schnittstelle, die Internetdienste haben sollten, dar.

Dies wird durch die  Prinzipien von REST verwirklicht:

#### Client-Server

Alle Eigenschaften der Client-Server-Architektur werden erfüllt. Der Server stellt einen Dienst, der Client fragt diesen an.

#### Zustandslosigkeit

Die Zustandslosigkeit ist die hervorstechendste der Prinzipien, im Vergleich zu anderen Paradigmen. Hierbei haben sowohl weder der Server noch der Client Zustandsinformation (states), z.B. Sessions oder Cookies, weswegen das Protokoll zustandslos ist (statelass). Alle für Server oder Client notwendige Informationen, damit diese REST-Nachrichten verstehen können, befinden sich in der REST-Nachricht selbst, sodass ein Speichern der Zustandsdaten zwischen Nachrichten überflüssig ist.
Damit ist jede Anfrage an den Server in sich geschlossen, da sie alle notwendigen Daten über den Zustand der Anwendung auf dem Client beinhaltet. Dies ermöglicht Lastenverteilung, da jede Anfrage von anderen separat von anderen einzeln betrachtet werden kann und zudem serverseitig keine Sitzungsverwaltung stattfindet.
Des Weiteren ist es ausfallsicher, da transaktionale Datenübertragung in einem singuläten Seitenaufruf durchgeführt wird.

#### Caching

HTTP Caching soll genutzt werden. Ferner werden Daten in Antworten auf Anfragen gekennzeichnet, ob sie gecacht werden können oder nicht. Ist dies der Fall, so wird einem Client Cache die Möglichkeit gegeben, die Antwortdaten für äquivalente Anfragen zu speichern.

#### Einheitliche Schnittstelle

Das Hauptunterscheidungsmerkmal von REST zu anderen Architekturen ist die einheitliche Schnittstelle, welche aus den vier nachfolgenden Punkten besteht.

##### Adressierbarkeit von Ressourcen

Jeder REST-konforme Dienst ist durch einen Uniform Resource Locator (URL) eindeutig identifiziert und standardisiert den Zugriff auf den Dienst.

##### Repräsentationen zur Veränderung von Ressourcen

Ressourcen können in verschiedenen Repräsentationen (JSON, XML, etc.) als Antwort zurückgeschickt werden.

##### Selbstbeschreibende Nachrichten

REST-Nachrichten sind selbstbeschreibend, einschließlich Standardmethoden über die sich Ressourcen verändern lassen.

##### Hypermedia as the Engine of Application State (HATEOAS)

HATEOAS ist ein Entwurfsprinzip von REST-Architekturen, dabei bewegt sich ein Client an einer REST-Schnittstelle nur mit Hilfe von URLs.

#### Mehrschichtige Systeme

Systeme werden mehrschichtig entworfen, wobei dem Nutzer nur eine Schnittstelle angeboten wird und alle anderen Ebenen verborgen bleiben.

#### Code on Demand (optional)

Code wird erst an einen Client übertragen, wenn dieser welchen anfragt.

### Jekyll

[Jekyll](https://jekyllrb.com/) ermöglicht die einfache Erstellung statischer Websites, insbesondere Blogs. In diesem Praktikum ist die Konfiguration und Erstellung der Website mithilfe eines Buildscripts automatisiert.

Alle für uns wichtigen Dateien liegen in unserem git-repository unter `Webseiten`. Die grundsätzliche Konfiguration erfolgt über die Datei `_config.yml`, wo das Theme, der Titel der Webseite und ähnliches festgelegt wird. Unterseiten können beispielsweise mit markdown angelegt werden, die dann von Jekyll zu HTML und CSS aus dem gewählten Theme zu einer (hoffentlich) gut aussehenden Webseite umgewandelt. Dabei wird folgender Header in der Markdown-Datei benötigt:

	---
	layout: page
	title: "Titel der Seite"
	---

Darüber hinaus ist Jekyll in der Lage auch Blogposts zu erstellen. Dafür legt man eine markdown-Datei nach folgendem Muster an: `_posts/JJJJ-MM-TT-name-des-posts`, wobei `JJJJ` das Jahr, `MM` den Monat und `TT` den Tag verdeutlicht. Außerdem ist auch hier ein Header erforderlich, der dieser Form folgen sollte:

	---
	layout: post
	title:  "Titel des Posts"
	categories: tags um den post zu beschreiben
	---

Ein Post dieser Art wird dann auf der Hauptseite unter `Posts` veröffentlicht.


### Frontend

Unter Frontend versteht man die Präsentationsebene einer Software, das heißt den Teil des Programms den der Betrachter sehen kann. Dazu können Bilder, Farben, Buttons, Schriften, Darstellungen und mehr gehören. Die Oberfläche wird meistens mit HTML, CSS und JavaScript gestalten. Die Aufgabe des Frontends ist es daher die Lücke zwischen Oberfläche und den Aktionen, die im Hintergrund erfolgen, zu schließen.


### HTML

HTML (HyperText Markup Language) ist eine plattformunabhängige und textbasierte Auszeichnungssprache. Auf ihr baut die Struktur auf, welche genutzt wird, um digitale Inhalte darzustellen. Zu diesen Inhalten können beispielsweise Bilder und Texte, aber auch Hyperlinks zählen. Die HTML-Dokumente werden mithilfe eines Browsers angezeigt.


### CSS

CSS (Cascading Style Sheets) ist eine Auszeichnungssprache mit der HTML- Dokumente formatiert werden können. Die Grundidee bei der Verwendung von Stylesheets ist die Trennung von Dateninhalten und deren Ausgabeform. Daraus ergeben sich eine Reihe an Vorteilen, z.B schnellere Updates des Layouts, die Möglichkeit zur dynamisch angepassten Ausgabe je nach Anzeigegerät und leichter wartbarer Quellcode


### JavaScript

JavaScript ist eine Skriptsprache und gehört zu den drei wichtigsten Sprachen für die Webentwicklung.  Sie programmiert das Verhalten von Webseiten und hat so die Möglichkeiten von HTML und CSS zu erweitern. Einer der wichtigsten Aufgaben von JavaScript ist es auf Benutzerinteraktionen zu reagieren und daraufhin Änderung im HTML-Dokument vorzunehmen.
JavaScript wird aber nicht nur in der Webentwicklung eingesetzt, sondern auch auf Servern und Desktops. Manche Datenbanken, wie zum Beispiel MongoDB, benutzen JavaScript als Programmiersprache.

### Python

Python ist eine multparadigmatische (u.a. objektorientiert, aspektorientiert, funktional), höhere interpretierte Programmiersprache, die eine dynamische Typisierung bietet.
Es wurde mit den Ziel entworfen einfach und übersichtlich zu sein, was auf der einen Seite durch den geringen Umfang von Schlüsselwörtern erreicht wird, und auf den anderen Seite durch die Runterbrechung der Syntax auf Einfachheit und Übersichtlichkeit.

### Pip

Pip (für "Pip installs packages") ist der empfohlene Paketmanager für Python. Die Pakete kommen dabei normalerweise aus dem Python Package Index (PyPI), können aber aus einer Reihe anderer Quellen (u.a. git-Repositories, Ordnern von Dateien und .zip-Dateien) installiert werden.
Außerdem kann eine Liste von Paketen und deren Versionen in einer "requirements" Datei verwaltet werden um schneller eine bestimmte Umgebung erzeugen zu können.

### Pipenv

Pipenv ist ein Tool um virtuelle Umgebung für Python Programme zu erzeugen, zu verwalten un um deren Nutzung zu vereinfachen. Pipenv baut dabei auf etablierten Programmen wie virtualenv, pip und pyenv auf.
Virtuelle Umgebung werden in Python genutzt um installierte Bibliotheken vom Rest des Systems zu isolieren. Damit wird erreicht, dass Abhängigkeiten nicht systemweit installiert werden müssen um die genutzte Umgebung minimal und damit einfacher wartbar zu halten. Außerdem können bestimmte Versionen des Python Interpreters genutzt werden (unabhängig von der systemweit genutzten Version) und unterschiedliche Versionen der gleichen Bibliothek auf dem gleichen Computer in verschiedenen Projekten existieren.
Abgesehen davon sorgt pipenv für deterministische builds. Das bedeutet, dass gewährleistet wird, dass reproduzierbar die gleichen Abhängigkeiten auf die gleiche Art installiert werden wodurch auf unterschiedlichen Systemen die gleiche Umgebung erzeugt werden kann.

### API

API steht für application programming interface, zu Deutsch Schnittstelle zur Anwendungsprogrammierung. Die API ist der Teil eines Softwareseystems, der anderen Programmen eine Anbindung an das System ermöglicht. Sie enthält Kommunikationsprotokolle, Werkzeuge und Definitionen von Unterprogrammen, die bei der Entwicklung eines Programmes helfen können. Es gibt APIs für Betriebssysteme, Datenbankensysteme, Softwarebibliotheken oder auch Webbasierte Systeme.

### URI

Uniform Resource Identifier, kurz URI, ist eine Zeichenfolge zur Identifierizierung von abstrakten oder physischen Ressourcen im Internet, vor allem im World Wide Web. Ressourcen sind dabei z.B. Webseiten oder der Aufruf von Webservices, können aber auch E-Mail-Empfänger sein. URIs werden dabei in digitale Dokumente, beispielweise solche im HTML-Format eingebunden.

Ein URI besteht nach dem aktuellen Standard RFC 3986 aus fünf verschiedenen Teilen:
* scheme (Schema oder Protokoll)
* authority (Server oder Anbieter)
* path (Pfad)
* query (Abfrage)
* fragment (Teil)

Nur scheme und path müssen in jedem URI enthalten sein. Ein Beispiel hierfür wäre

```
mailto:Max.Mustermann@beispiel.de
```
In diesem Beispiel ist 'mailto' der scheme und die E-Mail Adresse von Max Mustermann der path.


### Docker

> Docker vereinfacht die Bereitstellung von Anwendungen, weil sich Container, die alle nötigen Pakete enthalten, leicht als Dateien transportieren und installieren lassen. Container gewährleisten die Trennung und Verwaltung der auf einem Rechner genutzten Ressourcen. Das beinhaltet laut Aussage der Entwickler: Code, Laufzeitmodul, Systemwerkzeuge, Systembibliotheken – alles was auf einem Rechner installiert werden kann.
> <cite>https://de.wikipedia.org/wiki/Docker_(Software) (11.11.18 15:40)</cite>

Container (genauer die Images auf denen diese basieren) können dabei auch kombiniert oder auf Basis anderer Images erstellt werden. Die von der bereits vorhandenen abweichende Konfiguration und zusätzlich benötigte Dateien können dabei einfach in bestehende Images Integriert werden. Eine Anwendung kann dabei aus mehreren Images mit unterschiedlichem Inhalt (z.B. Webserver, Datenbanken, Load Balancer, …) bestehen und mit Docker verwaltet werden. Außerdem können auf Basis eines Images auch mehrere Container erzeugt werden um z.B. Redundanz oder Lastverteilung auf mehrere Maschinen zu erreichen.


### docker-compose

docker-compose ist ein auf Docker aufbauendes Tool zur Definition und Verwaltung mehrerer Container. Es Erlaubt das ausführen von Befehlen in mehreren Container gleichzeitig und stellt deren Log-Dateien zur einfacheren Betrachtung zusammen. Außerdem erlaubt es die Container über ein von außen nicht erreichbares Netzwerk miteinander zu verbinden wodurch z.B. Datenbanken von anderen Systemen und öffentlichem Zugriff isoliert werden können.


### Suchmaschine

Eine Suchmaschine ist ein Programm, das dazu dient, Informationen zu finden, die auf einem Computersystem gespeichert sind. Die Suchergebnisse werden normalerweise als eine Liste dargestellt und werden als Treffer bezeichnet. Suchmaschinen helfen dabei, die Zeit für das Auffinden von Informationen zu verkürzen und die Menge an Informationen, die abgefragt werden müssen, zu minimieren. Die wichtigsten Bestandteile sind, die Erstellung eines Indexes, die Verarbeitung einer Suchanfrage und die Aufarbeitung der Ergebnisse.


## Konzepte

### git flow

umfangreichere Erklärung: https://nvie.com/posts/a-successful-git-branching-model/

git flow ist ein Workflow design für git für eine übersichtliche Struktur des Repositories in komplexen Projekten mit mehreren Entwicklern.
Das Hauptmerkmal dabei ist die Nutzung verschiedener Branches:

- master definiert dabei den "offiziellen" Stand des Repos nach außen und ist i.a. eine stabile version des Projektes
- develop ist die Basis für die laufende Entwicklung
- Features werden in feature branches mit dem Präfix "feature/" ausgehend von develop entwickelt und bei Fertigstellung mit develop gemerged
- Hotfixes (kleine, kurzfristige Patches) werden ausgehen von master in branches mit Präfix "hotfix/" erzeugt und in master und develop gemerged


### RDF

Das Resource Description Framework ist eine Methode um logische Aussagen über beliebige Objekte (Ressourcen) zu äußern, dabei ähnelt es den Konzepten UML oder Entity-Relationship-Modell. Eine Aussage im RDF-Modell besteht aus den Elementen Subjekt, Prädikat und Objekt, wobei die Beziehung vom Subjekt zum Objekt gerichtet ist und mit dem Prädikat benannt wird. Diese Aussagen-Tripel bilden einen gerichteten Graphen. Wenn sich Tripel auf die gleichen Subjekte bzw. Objekte beziehen, bilden sie ein semantisches Netz, welches sich als Graph oder wie hier im Beispiel als Tabelle darstellen lässt:

|Subjekt          |   Prädikat           | Objekt     |
|:---------------:|  :------------------:|:----------:|
|Felix            |   plant              | Website    |
|Julia            |   programmiert       | Website    |
|Till             |   programmiert       | App        |

Ressourcen werden eindeutig mit URI's (Uniform Resource Identifier) bezeichnet, so ist es möglich, Aussagen verschiedener Quellen zu verknüpfen. Außerdem können Informationen vom Computer eigenständig interpretiert werden. Subjekte sind immer Ressourcen, doch das Objekt kann entweder eine Ressource oder ein Literal sein. Literale sind Strings, die noch mit einem angegebenen Datentyp interpretiert werden müssen z. B.  Zahlen, Datum.

Es gibt verschiedene Repräsentationsmöglichkeiten für RDF, üblich sind XML oder Notation3 (N3). In unserem Datensatz wird N3 verwendet, womit man Tripel einfach aufschreiben kann:
```python
<#Felix><#plant><#Website>.
<#Julia><#programmiert><#Website>.
<#Till><#programmiert><#App>.
```


### RVK (Regensburger Verbundklassifikation)

> Die Regensburger Verbundklassifikation (RVK, oft auch Regensburger Systematik) ist eine Klassifikation zur Erfassung von Beständen in Bibliotheken. Sie \[…\] hat sich in weiten Teilen Bayerns und darüber hinaus als Klassifikationssystem an wissenschaftlichen Bibliotheken (v.a. in Ostdeutschland, aber auch schon im Ausland) etabliert. Sie wird ständig gepflegt, die Redaktion liegt dabei bei der Universitätsbibliothek Regensburg. Es gibt eine Arbeitsgruppe für Systematikfragen, in der verschiedene Bibliotheken zusammenarbeiten.
> <cite> https://de.wikipedia.org/wiki/Regensburger_Verbundklassifikation (09.11.18 17:51) </cite>


### ORM (object relation mapper)

Ein ORM ist ein Tool um Objektrelationale Abbildungen zu erzeugen.

>Objektrelationale Abbildung (englisch object-relational mapping, ORM) ist eine Technik der Softwareentwicklung, mit der ein in einer objektorientierten Programmiersprache geschriebenes Anwendungsprogramm seine Objekte in einer relationalen Datenbank ablegen kann.
> <cite> https://de.wikipedia.org/wiki/Objektrelationale_Abbildung (09.11.18 21:28) </cite>

In Django sorgt der ORM für komfortable, objektorientierte Nutzbarkeit der Objekte in der Datenbank. Dies wird durch "Modelle" (spezielle Python Klassen über deren Instanzen der Zugriff auf die Datenbank geschieht) und von Django automatisch erstellten Hilfsdaten erreicht.


### Packaging
Das zusammenführen aller Dateien (Dokumentation, Quellen, ausführbare Programme, etc.) eines fertigen Projekts, in ein eindeutiges Objekt (z.B. CD, ISO, RPM) um sie dem Kunden bereitstellen zu können.


### User Experience

> Der Begriff User Experience (Abkürzung UX, deutsch wörtlich Nutzererfahrung, besser Nutzererlebnis oder Nutzungserlebnis – es wird auch häufig vom Anwendererlebnis gesprochen) umschreibt alle Aspekte der Erfahrungen eines Nutzers bei der Interaktion mit einem Produkt, Dienst, einer Umgebung oder Einrichtung.
> <cite>https://de.wikipedia.org/wiki/User_Experience (11.11.18 15:30)</cite>

Dabei geht in der regel vor allem um die bewusste Gestaltung des Umgangs der Nutzer eines Produktes mit diesen und der Steuerung des damit verbundenen Erlebnisses.


### Graphdatenbank

Graphdatenbanken nutzen im Gegensatz zu relationalen Datenbanken Graphen um zusammenhänge vernetzte Daten zu speichern. Dabei können sowohl die Knoten als auch die Kanten im Graphen Eigenschaften haben. Durch dieses Design wird die Abfrage stark vernetzter Daten beschleunigt und vereinfacht.
Abfragesprachen für Graphdatenbanken sind z.B. SPARQL, Cypher und Gremlin.


## Aspekte

### SPARQL

Grundlegende Anfragen in SPARQL werden auf RDF-Tripeln durchgeführt. Die Syntax weißt dabei Ähnlichkeit zu der von SQL auf.

#### Beispiel:

```
PREFIX ex: <http://example.com/>
SELECT ?author
WHERE {
  ?book ex:isInLibrary ex:Albertina.
  ?book ex:author ?author.
}
```

Diese Anfrage gibt aus einem beispielhaften Datensatz alle Autoren aus, die Bücher verfasst haben, welche in der Bibliotheca Albertina aufbewahrt werden.

Der Prefix ermöglicht eine Kurzschreibweise der URIs, um den Hauptanfrageteil übersichtlicher zu gestalten. Das Ergebnis der Anfrage wird durch die nach dem SELECT Keyword angegebenen Variablen bestimmt.
Variablen werden dabei mit einem **?** oder einem **$** gekennzeichnet. Als Variablen sind sowohl Subjekt und Objekt als auch das Prädikat der dreiteiligen RDF Struktur zulässig. Anfragen lassen sich auch durch Turtle-Abkürzungen mittels **,** und **;** verkürzen. Die folgende Anfrage ist zu der oberen Äquivalent.

```
PREFIX ex: <http://example.com/>
SELECT $author
WHERE {
  $book ex:isInLibrary ex:Albertina ;
        ex:author ?author.
}
```

### Objektorientierung

> Unter Objektorientierung (kurz OO) versteht man in der Entwicklung von Software eine Sichtweise auf komplexe Systeme, bei der ein System durch das Zusammenspiel kooperierender Objekte beschrieben wird. Der Begriff Objekt ist dabei unscharf gefasst; wichtig an einem Objekt ist nur, dass ihm bestimmte Attribute (Eigenschaften) und Methoden zugeordnet sind und dass es in der Lage ist, von anderen Objekten Nachrichten zu empfangen beziehungsweise an diese zu senden. Dabei muss ein Objekt nicht gegenständlich sein. Entscheidend ist, dass bei dem jeweiligen Objektbegriff eine sinnvolle und allgemein übliche Zuordnung möglich ist. Ergänzt wird dies durch das Konzept der Klasse, in der Objekte aufgrund ähnlicher Eigenschaften zusammengefasst werden. Ein Objekt wird im Programmcode als Instanz beziehungsweise Inkarnation einer Klasse definiert.
> <cite> https://de.wikipedia.org/wiki/Objektorientierung (11.11.18 14:45) </cite>

Wichtige Aspekte bei der Objektorientierten Programmierung sind Vererbung, Polymorphie und Kapselung.

Vererbung bezeichnet die Möglichkeit Eigenschaften und Methoden der Elternklasse(n) auf die Kindklasse zu übertragen. Diese können in der Kindklasse genutzt oder überschrieben werden. Durch Vererbung ist eine zunehmende Spezialisierung oder Abstraktion der Objekte einfach möglich und reduziert damit code duplication.

> Das Konzept der Polymorphie (Vielgestaltigkeit) bewirkt, dass Eigenschaften oder Methoden einer Klasse von Objekten referenziert werden können, ohne dass die konkrete Ausprägung in einem angesprochenen Objekt bekannt sein muss.
> Hinzu kommt mit der Aggregation die Unterscheidung zwischen dem Ganzen und seinen Teilen. Jedes Objekt im System kann als ein abstraktes Modell eines Akteurs betrachtet werden, der Aufträge erledigen, seinen Zustand berichten und ändern und mit den anderen Objekten im System kommunizieren kann, ohne offenlegen zu müssen, wie diese Fähigkeiten implementiert sind
> <cite> https://de.wikipedia.org/wiki/Objektorientierung (11.11.18 14:45) </cite>

Als Kapselung bezeichnet man die Abschottung der internen Logik von Objekten nach außen sodass diese nur über explizit definierte Schnittstellen zugänglich ist. Objekte können dadurch komplexe Funktionalität auf einfache Schnittstellen abstrahieren.


### Deployment

Unter deployment (deutsch: Softwareverteilung) werden eine Reihe von Aktivitäten zusammengefasst durch die eine Software auf einem System verfügbar gemacht wird.
Das Ausmaß und die Form dieser Aktivitäten können dabei von einer einfachen, manuellen Installation und Konfiguration der Software und allen benötigten Komponenten bis hin zu komplexen, voll automatisierten Tests, build-Prozessen mit Kompilierung und packaging sowie der Verteilung und Einrichtung reichen. Letzteres wird auch als continuous deployment bezeichnet.




## Quellen

- https://www.w3.org/TR/2013/REC-sparql11-overview-20130321/ [Abgerufen am 10.11.2018]
- http://dbis.informatik.uni-freiburg.de/content/courses/WS1011/Spezialvorlesung/Webbasierte%20Informationssysteme/folien/Vorlesung-SPARQL-Einfuehrung.pdf [Abgerufen am 10.11.2018]
- https://www.w3.org/DesignIssues/Notation3.html [Abgerufen am 7.11.2018]
- https://nats-www.informatik.uni-hamburg.de/pub/SemWeb/TeilnehmerBeitraege/rdf.pdf [Abgerufen am 7.11.2018]
