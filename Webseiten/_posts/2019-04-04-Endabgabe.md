---
layout: post
title: "Ende des Projektes, finale Demo ist online"
---

# Ende des Projektes, finale Demo ist online

## Download des finalen Releasebündels

[Download Endabgabe.zip](http://pcai042.informatik.uni-leipzig.de/~dm18a/Endabgabe.zip)

## Demo

[Link zur Demo](http://pcai042.informatik.uni-leipzig.de:1640)
