22.10.18 | dm18a | Tom Winter

# initiales Treffen vom 22.10.18

## Anwesenheit
- Vollständig

## Protokollant: Tom

- Beginn: 20:50
- Vorstellungsrunde, jeder commited seine Stärken/Schwächen zu git

## Wahl der Rollen
- Teamleitung: Felix (einstimmig)
- Stellvertretung: Tom (einstimmig)
- Protkollant: Victor (einstimmig)

## Präferierte Technologie
- Python mit Django

## Präferierte Rollenverteilung (vorläufig)
- Backend: Tom, Till, Felix, Magnus
- Frontend: Victor, Julia, Katha, Marius

## Themen für Recherchebericht
- Risikoanalyse: Katha, Julia
- RDF: Till, Victor
- SPARQL: Magnus, Marius
- Was ist "Semantische Bibliotheksservices", möglichst mit (fiktiven) Beispielen: Felix, Tom

## Erinnerung and den wöchentlich abzugebenden Aufwandsbericht

Inhalt:
- empfundene Schwierigkeit
- Zeitlicher Aufwand
- Inhalt/Thema der Tätigkeit

## Kommunikation im Team
- wir bleiben bei Slack

## Termin für Wöchentliches Treffen
- terminiert Felix mit Betreuer
- Gruppentreffen: Felix macht Doodle

## Sonstiges
- Tom: Wiki auf gitlab anlegen, Menschen hinzufügen

## Ende des Treffens
21:45
