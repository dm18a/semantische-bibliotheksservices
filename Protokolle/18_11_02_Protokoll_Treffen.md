# Protokoll vom Treffen am 02.11.18

## Allgemeines
- anwesend: Julia, Katha, Magnus, Till, Felix, Tom
- abwesend: Victor
- Protokoll: Tom
- Beginn: 15:10 Uhr
- Ende: 17:00 Uhr

## Ideensammlung

### mögliche Richtungen
- Verknüpfung über geografische Daten (siehe z.B. http://informatik.uni-leipzig.de:8080/MusikerProfiling/ )
- rewrite der existierenden Website
- Verknüpfung über (mögliche) Bekanntschaften
- citation graph (benötigt externe Daten!)
- Autorendatenbank mit Verfügbarkeitsabfrage für Werke
- Lokalisierung der Werke über Karten der Bibliothek
- Publikationsrate für Gebiete anzeigen
- Wunschliste für nicht vorhandende Bücher

### Konkretisierung der Idee
Seite ist um eine Suchmaschine herumgebaut, zusätzliche Features werden danach implementiert.

Gewünschte Features:
1. Verfügbarkeitsabfrage
2. Lokalisierung
3. zusätzliche Verknüpfungen einfügen um Suche zu verbessern

## Risikoanalyse
siehe Anhang

## Aufwandsbericht
Inhalt:
- Arbeitszeit
- Was wurde gemacht?

## Recherchebericht (+ relevante Bibliotheken)
Sortiert nach Begriffen, Konzepten und Aspekten. So viel dazu schrieben wie für grundlegendes Verständniss notwendig.

- Till, Victor: Jeckyll, RDF
- Magnus: SparQL, Slack
- Felix: Python, Visualiserung von Daten mit Python
- Julia, Katha: was bedeuten Ortungs-Tags für Bücher der Bibliotheken?, Raum-/Regalpläne der Bibliotheken, Frontend-Kram
- Tom: Django, git flow

Wenn bei der Recherche noch Definitionswürdiges auffällt -> dazu noch etwas schreiben.

## ToDo
- Alle: Mitteilung mit Arbeitszeit in Stunden und gefühlter Schwierigkeit von 1-5)
- Felix: Issues für Einrichtung der website (Till), und webserver (Tom)
- Alle: Ergebnisse des Rechercheberichts ins git laden
