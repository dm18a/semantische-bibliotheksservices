# Data And Access

* 2018-11-29, Martin Czygan <martin.czygan@uni-leipzig.de>

## IP whitelisting

We setup a SPARQL-Endpoint currently serving the *v0* dataset at:

    https://data.ub.uni-leipzig.de/fuseki

The base URL for the v0 dataset is:

    https://data.ub.uni-leipzig.de/fuseki/v0

To access the service, we need to whitelist your VPN IP address.

TODO(for you): Please send your Leipzig University VPN-IP [1] (172.x.x.x) for whitelisting to

        martin.czygan@uni-leipzig.de

## SPARQL Examples

* Spec: https://www.w3.org/TR/sparql11-overview/

A simple query:

```
SELECT ?s ?p ?o
WHERE {
	?s ?p ?o
}
LIMIT 10
```

The SPARQL endpoint is relatively restricted: SPARQL queries have to be
submitted via HTTP (GET or POST) request.

    $ curl -s http://data.ub.uni-leipzig.de/fuseki/v0/query?query=+SELECT+%3Fs+%3Fp+%3Fo+WHERE+%7B+%3Fs+%3Fp+%3Fo+.+%7D+LIMIT+10
        <?xml version="1.0"?>
        <sparql xmlns="http://www.w3.org/2005/sparql-results#">
          <head>
                <variable name="s"/>
                <variable name="p"/>
                <variable name="o"/>
          </head>
          <results>
                <result>
                  <binding name="s">
                        <uri>http://data.slub-dresden.de/resources/4367EEEF39F75770</uri>
                  </binding>
     ...

The output format can be controlled via HTTP header.

    $ curl -H 'Accept: application/json' http://data.ub.uni-leipzig.de/fuseki/v0/query?query=+SELECT+%3Fs+%3Fp+%3Fo+WHERE+%7B+%3Fs+%3Fp+%3Fo+.+%7D+LIMIT+10
        {
          "head": {
                "vars": [
                  "s",
                  "p",
                  "o"
                ]
          },
          "results": {
                "bindings": [
                  {
                        "s": {
                          "type": "uri",
                          "value": "http://data.slub-dresden.de/resources/4367EEEF39F75770"
                        },
                        "p": {
                          "type": "uri",
                          "value": "http://schema.org/inLanguage"
                        },
                        "o": {
        ...

### Using Python

Via [requests](http://docs.python-requests.org/en/master/).

```python
>>> import requests
>>> query = """
    ...: SELECT ?s ?p ?o
    ...: WHERE {
    ...:     ?s ?p ?o
    ...: }
    ...: LIMIT 2
    ...: """

>>> r = requests.get("https://data.ub.uni-leipzig.de/fuseki/v0/query", params={"query": query})
>>> assert r.status_code == 200
>>> r.json()
{'head': {'vars': ['s', 'p', 'o']},
 'results': {'bindings': [{'s': {'type': 'uri',
     'value': 'http://data.slub-dresden.de/resources/4367EEEF39F75770'},
    'p': {'type': 'uri', 'value': 'http://schema.org/inLanguage'},
    'o': {'type': 'literal', 'value': 'ger'}},
   {'s': {'type': 'uri',
     'value': 'http://data.slub-dresden.de/resources/4367EEEF39F75770'},
    'p': {'type': 'uri', 'value': 'http://schema.org/sameAs'},
    'o': {'type': 'uri',
     'value': 'http://swb.bsz-bw.de/DB=2.1/PPNSET?PPN=0018609131'}}]}}
```

### Server spec

The server runs CENTOS 7.5.1804 (3.10.0-862.9.1.el7.x86_64) and FUSEKI 3.9.0
[2][3]; the data sets have been created via `tdb2.tdbloader` [4]. 8 CPU,
E5-2660, 32G DDR3.

## WACHTL Service

WACHTL is an internal middleware service bridging a widely use library
management system (called LIBERO) with other services. Especially, there is an
API provided by WACHTL, that allows to query for availability status of items:

Note: Not all IDs in the dataset are in the possession of the Leipzig
University Library.

The next finds a single, random WACHTL URL from the first 100000 lines of the
`data-v0.nt` input file and runs a HTTP GET against the API. The `jq` [5] tool
is optional, but useful.

```
$ curl -H 'Accept: application/ld+json' -vL $(grep -Eo 'http://[^ ]*wachtl[^ >]*' data-v0.nt | head -100000 | shuf -n 1) | jq .
{
  "http://purl.org/ontology/daia/collectedBy": {
    "@type": "http://xmlns.com/foaf/0.1/Organization",
    "http://schema.org/url": "https://www.ub.uni-leipzig.de/",
    "http://schema.org/description": "Universitätsbibliothek Leipzig"
  },
  "http://purl.org/ontology/daia/timestamp": "2018-10-30T14:33:10.234+01:00"
}
```

Here's a link that leads to some information, the response is serialized as JSON-LD:

```
$ curl -H 'Accept: application/ld+json' -vL http://data.ub.uni-leipzig.de/item/wachtl/DE-15:ppn:030282004 | jq .
{
    "data.ub.uni-leipzig.de/item/wachtl/showsAvailabilityOf": [
        {
            "@type": "http://purl.org/ontology/bibo/Document",
            "@id": "DE-15:ppn:030282004",
            "http://schema.org/url": "https://katalog.ub.uni-leipzig.de/Record/0003307141",
            "http://purl.org/ontology/daia/exemplar": [
                {
                    "@type": "http://purl.org/vocab/frbr/core#Item",
                    "@id": "DE-15:ppn:030282004:0016220998",
                    "http://schema.org/url": "https://katalog.ub.uni-leipzig.de/Record/0003307141",
                    "http://purl.org/ontology/daia/label": "01A-2008-19439",
                    "http://purl.org/ontology/daia/heldBy": {
                        "@type": "http://xmlns.com/foaf/0.1/Organization",
                        "@id": "http://data.ub.uni-leipzig.de/resource/DE-15/department/zw01",
                        "http://schema.org/description": "Bibliotheca Albertina"
                    },
                    "http://purl.org/ontology/daia/unavailableFor": [
                        {
                            "@type": "http://purl.org/ontology/daia/Service/Presentation"
                        },
                        {
                            "@type": "http://purl.org/ontology/daia/Service/Loan"
                        }
                    ],
                    "data.ub.uni-leipzig.de/item/wachtl/hasStatus": "L\u00e4ngerfristige Ausleihe, bitte wenden Sie sich an das Personal"
                }
            ]
        }
    ],
    "@type": "http://purl.org/ontology/daia/Response",
    "http://purl.org/ontology/daia/collectedBy": {
        "@type": "http://xmlns.com/foaf/0.1/Organization",
        "http://schema.org/url": "https://www.ub.uni-leipzig.de/",
        "http://schema.org/description": "Universit\u00e4tsbibliothek Leipzig"
    },
    "http://purl.org/ontology/daia/timestamp": "2018-10-30T14:37:50.979+01:00"
}
```

The availability spec is called [DAIA](https://verbundwiki.gbv.de/display/VZG/DAIA).

> Die Document Availability Information API (DAIA) ist eine auf HTTP und JSON
> basierende Schnittstelle um aktuellen Verfügbarkeitsinformationen zu
> Dokumenten in Bibliotheken und ähnlichen Einrichtungen abzufragen. DAIA ist
> unter <http://purl.org/NET/DAIA> spezifiziert und in verschiedenen Servern
> und Clients implementiert.


## Master Data (Stammdaten)

* TODO(martin): Add dataset location.

----

* [1] https://www.urz.uni-leipzig.de/dienste/netze-zugang/vpn/
* [2] https://jena.apache.org/documentation/fuseki2/
* [3] https://github.com/miku/fusekibundle
* [4] https://jena.apache.org/documentation/tdb2/tdb2_cmds.html
* [5] https://stedolan.github.io/jq/

